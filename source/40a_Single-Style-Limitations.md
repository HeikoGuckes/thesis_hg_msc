\renewcommand{\el}{\mathcal{L}}
\renewcommand{\els}{\el_{s}}
\renewcommand{\elc}{\el_{c}}
\renewcommand{\ls}{\lambda_{s}}
\renewcommand{\lc}{\lambda_{c}}
\newcommand{\ltv}{\lambda_{tv}}
\renewcommand{\contentlayers}{C}
\renewcommand{\stylelayers}{S}



\newpage




# Evaluation of the Existing Architecture {#sec:limitations}

In this section we describe two experiments to assess the capabilities of the single-style network architecture for multi-style.
Instead of using multiple content images and a single style as input the first experiment uses multiple style images and a single content image as input (see [@Sec:reversed-approach]). This should give us an idea about the general applicability of the single-style network architecture for multi-style. The results lead us to the second experiment in [@Sec:concatenation-approach].
Before we elaborate on the experiments, we introduce the single-style architecture in [@Sec:single-style-architecture], outline the general methodology in [@Sec:general-methodology] and the used datasets and preprocessing methods in [@Sec:datasets].

The term architecture in this thesis always refers to the architecture of the neural network. The system setup is the combination of the input images, the neural network, the loss function, and potential extensions.


## Single-Style Architecture {#sec:single-style-architecture}


The architecture for the single-style image transformation network is the same as [@google2016] with initial reflection padding or [@Johnson] with three key modifications that are not related to multi-style:

1) To avoid border-artifacts we use reflection padding instead of zero padding. Our neural network library [@lasagne] misses low level support for reflection padding. Therefore, we resorted to a slow high level implementation and chose to do one big initial reflection padding instead of a small one at every convolution to keep performance at a reasonable level. This should not reflect results at all but the architecture looks slightly more complicated due to some additional cropping layers.

2) To avoid checkerboard artifacts we replace transposed convolution, also known as deconvolution with nearest-neighbor upscaling followed by a convolution. The checkerboard artifacts originate from the zeroes in the upscaling step within the transposed convolutions [@checkerboard].

3) To improve visual quality we replace batch normalization with instance normalization, which applies normalization to single images instead of a whole batch of images [@instance].

With these changes we agree with [@google2016] that the total variation loss is no longer necessary for training the feed-forward networks.

This leads to the architecture in [@Tbl:single-style-architecture] with strided convolutions for downsampling and factor two nearest-neighbor upsampling together with a following convolution for upsampling. In between are five residual blocks [@residual] with the architecture from @residual-architecture. A residual block consists of two convolution layers where the second one has no nonlinearity. Before the result of the convolutions and the input to the block can be added, the input needs to be cropped. This stems from the initial reflection padding and no padding for the convolutions (VALID padding). We summarized the training parameters in [@Tbl:single-style-architecture-parameters].

All convolutional layers are followed by instance normalization [@instance] and ReLU nonlinearities. The output layer is the exception and uses no instance normalization and no ReLU but uses a scaled tanh to ensure the range of [0, 255] for the output pixels.   
The first and last layer use size $9 \times 9$ kernels while all others are $3 \times 3$. The $3 \times 3$ kernel downsampling convolutions use stride 2 while all others use stride 1.   
For padding we use an initial reflection padding with width 54 while layers
themselves do not use any padding for performance reasons.   
We consider this a state of the art single-style network architecture and use it as a baseline for the multi-style experiments.

<!-- \begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{source/architecture/upsampling.png}
  \caption[Comparison of upsampling techniques]{Upsampling techniques: The original feature map (left), upsampled with transposed convolution (middle) and nearest-neighbor upsampling (right).}
  \label{fig:upsampling}
\end{figure} -->
