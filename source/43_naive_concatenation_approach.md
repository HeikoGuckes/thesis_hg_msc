\renewcommand{\el}{\mathcal{L}}

\newpage

## The Concatenation Approach: Multi-Style Network {#sec:concatenation-approach}
<!--  --------------------------------------------------------------------  -->

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{source/architecture/MultiNet.pdf}
  \caption[System overview for training the concatenation multi-style networks]
          {System overview for training the \dd*concatenation* multi-style transfer networks. The transformation network has style and content as its input.}
  \label{fig:multi-style-architectural-overview}
\end{figure}


We learned that a single-content network has sometimes problems to get close to the content. The *single-style network* never had this problem as it has direct access to the content image from its input. Therefore, we want to add the content image as an input to the network to alleviate the problem. In consequence the network does not need to infer the content from the loss function during training. We want to improve the content loss and get a clearer representation of the content image within the pastiche. We are also interested in what happens when we start to train for multiple content images and multiple styles as this is what this thesis is about.


<!--  ---------------------------------------  -->
\subparagraph*{Concatenation Multi-style Network Architecture}\nop{*}
The *single-style network* $f_w(c)$ takes one image with content image $c$ as input. The multi-style architecture extends this to $f_w(c, s)$ and simply adds a **concatenation layer** that concatenates the second input $s$ of size $C_s \times W \times H$ and the content image with size $3 \times W \times H$. $C_s$ is the number of channels of the second input $s$. For normal color images $C_s$ is $3$. The concatenation layer feeds the resulting tensor of size $(3+C_s) \times W \times H$ into the otherwise unchanged network. The system overview is shown in [@Fig:multi-style-architectural-overview]. In consequence, training minimizes the modified loss function:

$$
W_s^{*} = \arg\min_W \mathbb{E}_{ s, c }
\biggl[  
   \el(s, c, f_W(s, c))
\biggr]
$$ {#eq:multi-style-training}


<!--  ---------------------------------------  -->
\subparagraph*{Method}\nop{*}

We want to conduct three experiments with increasing problem complexity to figure out the limitations. We start with single-content plus ten-styles, move to 80k content images plus ten-styles to 80k content plus 80k style images. All three experiments use the concatenation multi-style architecture.

First we redo the experiment from the single-content approach [@Sec:reversed-approach] and keep all parameters equal to see if the content representation within the pastiches improves. We still restrict the content $c$ to the *Chicago* content image. The only difference is that content and style are fed into the transformation network. We call the resulting network the *chicago-concatenation-ten-styles network*. It is a function $f_W(s, c_{chicago})$ where $s \in S_{Ten}$.

Second we increase the number of content images by using the *COCO Content Dataset*. Therefore, we redo the experiment from single-style, but additionally we will try to train for multiple styles by feeding the style images from our *Ten-Styles Dataset* into the network. For this the first layer concatenates a batch of four different training images from the *COCO Content Dataset* with a batch of four different training images from the *Ten-Styles Dataset*. We draw from both datasets in sequential order.
<!-- MAYBE: why not random? -->
We call the resulting network the *ten-styles-concatenation network*. It is function $f_W(s, c)$ where $s \in S_{Ten}$ and arbitrary content image $c$.

The third experiment mirrors the second in every aspect except that it uses the roughly 80k style images from the *Painters Dataset* instead of only ten. We call the resulting network the *painters-concatenation network*.


<!--  ---------------------------------------  -->
\subparagraph*{Results}\nop{*}

The *chicago-concatenation-ten-styles* network from our first experiment produces images with a much clearer content representation compared to the *chicago-inferred-ten-styles* network (single-content). This can be seen in  [@Fig:images-concat-vs-reversed] for the *Mosaic* and *Composition* style images, where the *chicago-inferred-ten-styles* network had the biggest problems carving out the content images. The resulting losses agree with our observation as style and content loss are both lower when the network has the content image available at its input (see [@Fig:single-content-vs-concatenation-concatenation-has-better-loss]).



For the *ten-styles-concatenation network* from our second experiment [@Fig:images-concat-mean-artifacts] visualizes the *style-copy-artifacts* problem: When learned with multiple styles, some elements are copied into every pastiche regardless of the content image. The problem becomes especially apparent when looking at the mean image over multiple pastiches generated with the same style image. We can also set the non-similar pixels in each color channel to 0 and the similar colors ($\pm 3$) to 255 to get an image representing the invariance over the three content images. We measure the percentage of similar (±3) pixels in each color channel over 30 pastiches: For every style from our *ten-styles dataset* over pastiches generated with three different content images. This network copies about 12.6\% similar pixels into every pastiche for the *Candy* style and an average of 7.3\% for all ten styles. For comparison, the *single-style network* copies about 0.28\% for the *Candy* style and an average of 0.53\% for all ten styles.    
The artifacts occur very early during training, even after 1k iterations they are clearly visible as shown in [@fig:concatenation-ten-styles-style-copy-origin].
More pastiches from the *ten-styles-concatenation network* can be found in Fig. \ref{fig:naive-blending-problem} for all of the ten styles.

Regarding the losses for all styles in the *ten-styles dataset* (Shown in [@Fig:naive-vs-single-naive-has-better-losses]) we see a better (lower) or equal style loss for the *ten-styles-concatenation network*. This shows that *style copy artifacts* together with *weak styling* leads to a very good style loss. We consider a style transfer *weak* when mostly colors are transferred and the pastiche is lacking most of the typical structures found in the pastiches of the *single-style network*. We can also see that the style loss is about a magnitude higher for the first few hundred iterations.

For the *painters-concatenation network* that tries to learn 80k styles we see the *style-blend* problem in [@fig:concatenation-painters-shows-style-blending]. Instead of styling the content image with the style, the network produces a blended version of both images. In some cases the colors are modified (*weak* styling).


 \begin{figure}
   \centering
   \includegraphics[height=0.24\textheight]{source/results/naive/plotCompareWithSingleContent/plot_Content_loss_mean_over_Ten-Styles.pdf}
   \hfill
   \includegraphics[height=0.24\textheight]{source/results/naive/plotCompareWithSingleContent/plot_Style_loss_mean_over_Ten-Styles.pdf}
   \caption[Losses of the concatenation approach compared to the single-content network]{     
     Content loss (left) and style loss (right).
     %Both with only \dd*Chicago* as content image and the \dd*Ten-Styles Dataset* as style images.
     Adding the content image as an additional input to the network (\dd*chicago-concatenation-ten-styles*) clearly improves both loss types compared to the \dd*chicago-inferred-ten-styles* and surpasses even the \dd*single-style baseline*.
   }
   \label{fig:single-content-vs-concatenation-concatenation-has-better-loss}
 \end{figure}




 \begin{figure}
   \centering
   \includegraphics[height=0.24\textheight]{source/results/plots_TenStyles/naiveVSsingle/plot_Loss_mean_on_the_validation_set_average.pdf}
   %\includegraphics[height=0.24\textheight]{source/results/plots_TenStyles/naiveVSsingle/plot_Loss_mean_on_the_validation_set_wave.pdf}
  \includegraphics[height=0.24\textheight]{source/results/plots_painters/naiveVSsingle/plot_Loss_mean_on_the_validation_set_average.pdf}


   \caption[Losses of the concatenation approach compared to single-style baseline]{     
     Comparison of the \dd*concatenation-ten-styles* network and the \dd*single-style baseline* (left plot). For all styles in the \dd*Ten-Styles Dataset* we see a lower or equal style loss for the concatenation approach. This shows that \dd*style copy artifacts* combined with some color transfer is a good strategy from the perspective of the style loss. The total loss is roughly equal to the desired output from the \dd*single-style networks*. The style loss is far higher for the first few hundred iterations.
     The \dd*painters-concatenation network* strategy of blending style and content image together produces far worse losses overall (right plot).
   }
   \label{fig:naive-vs-single-naive-has-better-losses}
 \end{figure}
