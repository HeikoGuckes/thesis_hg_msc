

\renewcommand{\mpwidth}{2.70cm}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  %\inclimg{#1#2candy.png}%
  %\inclimg{#1#2composition_vii.png}%
  \inclimg{#1#2famosas-crop.png}%
  \inclimg{#1#2wave.png}%
  %\inclimg{#1#2feathers.png}%
  %\inclimg{#1#2la_muse.png}%
  %\inclimg{#1#2mosaic.png}%
  %\inclimg{#1#2starry_night.png}%
  %\inclimg{#1#2the_scream.png}%
  %\inclimg{#1#2udnie.png}%
}

\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.275cm}%
      \\}

\begin{figure}[!htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}

  \newcommand{\combined}{source/results/painters/combine_painters_40k/}
  \newcommand{\naive}{source/results/painters/naive_painters40k/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.8cm}%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}}}

  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\istyles}{}
    \captionv{Style image}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \inclimg{source/images/content/chicago.png}
    \captionv{Content Image}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\combined}{chicagoX}
    \captionv{Combined}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\naive}{chicagoX}
    \captionv{Concatenation}  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\combined}{meanX}
    \captionv{Mean Combined}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\naive}{meanX}
    \captionv{Mean Concatenation}  
  \end{minipage}
  \hfill

  \caption[Comparison of the combined and concatenation approach trained for 80k style images]{  
   The combined approach and the concatenation approach trained for the 80k style images from the \dd*Painters Dataset*. Both regress to transferring color and blending style and content image.
  }

  \label{fig:visual-combined-approach-painters}
\end{figure}
