<!-- ### Losses -->

\subparagraph*{Losses}\nop{*}

The concatenation approach has the best style loss and the worst content loss of all methods as shown in [@Tbl:losses-ten-styles-all-methods]. It uses artifacts inspired by the style image that probably have a very low style loss and copies them into every pastiche, which explains the low style loss. Since these artifacts ignore the content completely, the high style loss is also explained. This tells us that the perceptual loss function has limited validity and should not be viewed in isolation.

Except for the supervision approach at 40k and 80k and the iterative method, the average total losses are within 5% of each other and vary between 4 and 5% for different content images. All content losses are within 8% of each other and vary between 4 and 6% over different content images. The style losses vary more and are within 20% of each other. They vary between 7 and 9% for different content images. Even the lowest feedforward losses are more than 30% higher than those from the iterative method. This method has the advantage that it individually optimizes each style and content loss combination. For the same reason it varies more for different content images than the feedforward approaches.

Even with 120k iterations the supervised method has a 33% higher style loss than the single-style reference and requires more than 40k updates to achieve a better content loss. This is consistent with the slight blurriness within the pastiches. The combined approach reaches the level of single-style networks at around 60k iterations for the style-loss and reaches even better values for the content-loss. This is not unexpected as the content-loss is prioritized at the start of the training, but we were surprised by the good style loss.

We summarize that our approaches are very close to the loss levels of the single-style method, but this says nothing about the perceived quality of the results as the concatenation approach demonstrates.

<!--Content loss 8%: 2,43/((2,42+2,08)/2) -->
<!--Style loss : 1,25/((1,24+0,86)/2) -->

 Method                                 Updates         Style loss 1e5     Content Loss $1e5$         Total Loss 1e5   
 ----------------------  ---------------------- ---------------------- ---------------------- ----------------------
 \Isupervised                               40k        $2.02 \pm 9\%$        $2.35 \pm 5\%$        $4.38 \pm 5\%$
 \Isupervised                               80k        $1.54 \pm 7\%$        $2.15 \pm 6\%$        $3.61 \pm 4\%$
 \Isupervised                              120k        $1.25 \pm 9\%$        $2.08 \pm 6\%$        $3.41 \pm 5\%$
 \Icombined                                 40k        $1.13 \pm 9\%$        $2.24 \pm 6\%$        $3.38 \pm 5\%$
 \Icombined                                 60k        $0.95 \pm 8\%$        $2.22 \pm 6\%$        $3.18 \pm 5\%$
 \Inaive                                    40k        $0.86 \pm 8\%$        $2.42 \pm 4\%$        $3.29 \pm 4\%$
 \Isingle                                   40k        $0.94 \pm 7\%$        $2.30 \pm 6\%$        $3.23 \pm 5\%$
 \Iiterative                                500       $0.65 \pm 24\%$       $1.31 \pm 21\%$       $1.95 \pm 23\%$
 -----------------------------------------------
 Table: Loss comparison of our novel approaches averaged over ten different styles with the different references including standard deviation over 128 content images. {#tbl:losses-ten-styles-all-methods}


 <!--
  Method                                 Updates         Style loss 1e5     Content Loss $1e5$         Total Loss 1e5   
  ----------------------  ---------------------- ---------------------- ---------------------- ----------------------
  \Isupervised                               40k        $2.02 \pm 0.19$        $2.35 \pm 0.11$        $4.38 \pm 0.20$
  \Isupervised                               80k        $1.54 \pm 0.11$        $2.15 \pm 0.13$        $3.61 \pm 0.16$
  \Isupervised                              120k        $1.25 \pm 0.11$        $2.08 \pm 0.13$        $3.41 \pm 0.16$
  \Icombined                                 40k        $1.13 \pm 0.10$        $2.24 \pm 0.14$        $3.38 \pm 0.17$
  \Icombined                                 60k        $0.95 \pm 0.08$        $2.22 \pm 0.13$        $3.18 \pm 0.15$
  \Inaive                                    40k        $0.86 \pm 0.07$        $2.42 \pm 0.10$        $3.29 \pm 0.13$
  \Isingle                                   40k        $0.94 \pm 0.07$        $2.30 \pm 0.14$        $3.23 \pm 0.17$
  \Iiterative                                   40k        $0.41 \pm 0.08$        $2.30 \pm 0.14$        $3.23 \pm 0.17$

  -----------------------------------------------
  Table: Loss comparison of our novel approaches averaged over ten different styles with the different references including standard deviation over 128 content images. {#tbl:losses-ten-styles-all-methods} -->



<!-- [@fig:plot-all-five-methods]

\begin{figure}
  \centering
  \includegraphics[width=0.49\textwidth]{source/results/plots_TenStyles/allFive/plot_Loss_mean_on_the_validation_set_average_upTo60k_ls_lc.pdf}
  \caption[Plot from the Losses of All Methods]{All methods in one plot}
  \label{fig:plot-all-five-methods}
\end{figure} -->

<!--
040000/0   323627.6=sum    94002.9=ls   229624.7=lc       -1.0=lref        0.0=ltv  vmean:single
040000/0    17280.1=sum     7078.2=ls    14637.4=lc        0.0=lref        0.0=ltv   vstd:single
060000/0   306106.4=sum    86624.7=ls   219481.8=lc       -1.0=lref        0.0=ltv  vmean:single
060000/0    16955.0=sum     6214.9=ls    14680.9=lc        0.0=lref        0.0=ltv   vstd:single
080000/0   296697.6=sum    81929.5=ls   214768.1=lc       -1.0=lref        0.0=ltv  vmean:single
080000/0    17047.6=sum     5336.1=ls    15154.3=lc        0.0=lref        0.0=ltv   vstd:single

040000/0   323122.3=sum    92388.3=ls   230734.0=lc   187179.1=lref        0.0=ltv  vmean:penalize
040000/0    14124.4=sum     6472.6=ls    10711.4=lc     1155.0=lref        0.0=ltv   vstd:penalize
060000/0   303836.7=sum    85123.0=ls   218713.8=lc   181299.9=lref        0.0=ltv  vmean:penalize
060000/0    14109.0=sum     5810.9=ls    10942.7=lc     1274.5=lref        0.0=ltv   vstd:penalize

040000/0   328570.7=sum    86405.4=ls   242165.3=lc        0.0=lref        0.0=ltv  vmean:concatenation
040000/0    13042.2=sum     6758.0=ls    10278.3=lc        0.0=lref        0.0=ltv   vstd:concatenation
060000/0   309800.0=sum    77406.1=ls   232394.0=lc        0.0=lref        0.0=ltv  vmean:concatenation
060000/0    12359.2=sum     5439.2=ls    10610.0=lc        0.0=lref        0.0=ltv   vstd:concatenation
080000/0   301580.5=sum    73565.7=ls   228014.8=lc        0.0=lref        0.0=ltv  vmean:concatenation
080000/0    12074.1=sum     4666.8=ls    10424.1=lc        0.0=lref        0.0=ltv   vstd:concatenation

040000/0   437638.7=sum   202960.0=ls   234678.7=lc   133402.5=lref        0.0=ltv  vmean:supervision
040000/0    19892.5=sum    19328.4=ls    11427.2=lc    11240.5=lref        0.0=ltv   vstd:supervision
060000/0   389091.8=sum   172813.5=ls   216278.2=lc   101793.4=lref        0.0=ltv  vmean:supervision
060000/0    17065.6=sum    14359.9=ls    12082.2=lc    10153.2=lref        0.0=ltv   vstd:supervision
080000/0   341210.2=sum   125451.6=ls   215758.5=lc    86217.5=lref        0.0=ltv  vmean:supervision
080000/0    15828.2=sum    10803.4=ls    12879.9=lc     9482.7=lref        0.0=ltv   vstd:supervision
120000/0   361780.8=sum   154253.0=ls   207527.8=lc    74652.3=lref        0.0=ltv  vmean:supervision
120000/0    16480.2=sum    11038.4=ls    13458.7=lc     9657.8=lref        0.0=ltv   vstd:supervision

040000/0   317374.5=sum   104983.6=ls   212390.9=lc        0.0=lref        0.0=ltv  vmean:controlled
040000/0    14386.8=sum     8743.0=ls    14175.2=lc        0.0=lref        0.0=ltv   vstd:controlled
060000/0   295580.4=sum    89222.7=ls   206357.7=lc        0.0=lref        0.0=ltv  vmean:controlled
060000/0    13866.9=sum     6597.0=ls    13633.3=lc        0.0=lref        0.0=ltv   vstd:controlled
080000/0   290345.9=sum    87017.2=ls   203328.7=lc        0.0=lref        0.0=ltv  vmean:controlled
080000/0    14018.4=sum     5895.5=ls    13536.1=lc        0.0=lref        0.0=ltv   vstd:controlled

040000/0   337729.7=sum   113335.8=ls   224393.9=lc   201479.0=lref        0.0=ltv  vmean:combined
040000/0    17082.8=sum     9809.4=ls    13761.1=lc     1182.1=lref        0.0=ltv   vstd:combined
060000/0   317778.8=sum    95997.9=ls   221780.9=lc   201605.8=lref        0.0=ltv  vmean:combined
060000/0    15225.5=sum     7770.9=ls    13064.8=lc      934.6=lref        0.0=ltv   vstd:combined
-->
