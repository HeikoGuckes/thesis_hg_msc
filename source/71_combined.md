\renewcommand{\el}{\mathcal{L}}
\renewcommand{\els}{\el_{s}}
\renewcommand{\elc}{\el_{c}}
\renewcommand{\ls}{\lambda_{s}}
\renewcommand{\lc}{\lambda_{c}}
\renewcommand{\contentlayers}{C}
\renewcommand{\stylelayers}{S}

\renewcommand{\lb}{\lambda_{b}}
\renewcommand{\lref}{\lambda_{ref}}

## Evaluation of the Combined Approach for Ten Styles
<!--  --------------------------------------------  -->
For this approach we combined two ideas. Controlling the content loss is supposed to reduce the artifacts. Penalizing the style blending by encouraging difference to the style image is supposed to prevent pastiches close to the style image. Are both building blocks necessary?

\subparagraph*{Visual}\nop{*} In [@fig:visual-combined-approach] the \dd*Famosa* Style (bottom row) on the \dd*Hoovertower* content demonstrates the effect of penalizing equality to the style image (5th column). The strong hair inspired structures from the Concatenation method (6th column) do not appear, instead we can see faint white in the area for the penalizing approach.   
Controlling the content loss shows its strength with regard to the tower itself (4th column, top row) as its roof is clearly above the structure in the top right of the \dd*Candy* style. The same image shows that the control approach leads to big parts of the style image appearing in the background.   
The penalizing approach shows its true capabilities when combined with the controlling approach (3rd column) as it encourages the image to deviate from the style image, which leads to a completely different pastiche for the \dd*Candy* style. The only element hinting at the structure of the original style is the high contrasting circle-like element that reappears with different colors. This problem might be stronger for different styles. We can see how the background is slightly deviated from the \dd*Famosa* image (3rd column), especially compared to the control variant (4th column), where a woman's face is recognizable.

\renewcommand{\mpwidth}{2.70cm}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%
  %\inclimg{#1#2composition_vii.png}%
  \inclimg{#1#2famosas-crop.png}%
  %\inclimg{#1#2wave.png}%
  %\inclimg{#1#2feathers.png}%
  %\inclimg{#1#2la_muse.png}%
  %\inclimg{#1#2mosaic.png}%
  %\inclimg{#1#2starry_night.png}%
  %\inclimg{#1#2the_scream.png}%
  %\inclimg{#1#2udnie.png}%
}

\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.275cm}%
      \\}

\begin{figure}[h]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\combined}{source/results/combined/combine_punish_lc12to1_TenStyles40k/}  
  \newcommand{\linlc}{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/}
  \newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}  
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.8cm}%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}}}

  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\istyles}{}
    \captionv{Style image}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/hoovertowernight.png}
    \inclimg{source/images/content/hoovertowernight.png}
    \captionv{Content image}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\combined}{hoovertowernightX}
    \captionv{Combined $\el_{b}+\elc$}  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\linlc}{hoovertowernightX}
    \captionv{$\elc$-controlled}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\punish}{hoovertowernightX}
    \captionv{$\el_{b}$-penalize}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\naive}{hoovertowernightX}
    \captionv{Concatenation}  
  \end{minipage}
  \hfill

  \caption[Pastiches from the combined approach and its individual components]{  
   Pastiches from the \dd*combined* approach and its individual components.
  }

  \label{fig:visual-combined-approach}
\end{figure}

<!-- The effect from the penalizing approach alone is not that interesting as most pastiches consist of some artifacts combined with primarily color transfer and without the style elements the single-style network shows. This is demonstrated by the *Candy* style in [@fig:visual-combined-approach] (4th column). -->





<!--  ---------------------------------------  -->
\subparagraph*{Artifacts}\nop{*}
In [@fig:images-combined-VS-concat-artifacts] we can see that the control and penalize approach reduce the *style copy artifacts* for the *Candy* style. The combined approach produces even better results. It reduces similar pixels to 1.6% compared to 12.6% of the concatenation approach for the candy style and from 7.3% to 1.6% averaged over all ten styles.



\renewcommand{\mpwidth}{2.70cm}
\renewcommand{\mphspace}{\hspace{-0cm}}

\newcommand{\inclimgNF}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.875cm}%
      \\ %                
}

\renewcommand{\inclimg}[2][]{%
  \frame{%
    \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
        {#2}%
      }%
        \\ \vspace{-0.875cm}%
        \\ %

}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2composition_vii.png}%  
}

\renewcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%
}

\begin{figure}[b]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}
  \newcommand{\combined}{source/results/combined/combine_punish_lc12to1_TenStyles40k/}  
  \newcommand{\linlc}{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.26cm}%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}}}

  \begin{minipage}{\mpwidth}
    \inclimgNF{source/images/styles/candy.png}
    \captionv{Candy Style}  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\combined}{meanX}
    \captionv{Mean Combined}
	\end{minipage}  
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\combined}{artifactsX}
    \captionv{Combined $\el_{b}+\elc$}
	\end{minipage}  
  \hfill
  \hspace{0.015cm}
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\linlc}{artifactsX}
    \captionv{$\elc$-controlled}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\punish}{artifactsX}
    \captionv{$\el_{b}$-penalize}   
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\naive}{artifactsX}
    \captionv{Concatenation}  
	\end{minipage}
  \hfill

  \caption[Artifact comparison of the combined approach and its individual components]{      
      Artifact comparison of the combined approach and its individual components.
    }

  \label{fig:images-combined-VS-concat-artifacts}
\end{figure}




<!--  ---------------------------------------  -->
\subparagraph*{Losses}\nop{*}

Controlling the content loss to be high ($\lc = 12$) at the beginning leads to a much better balance between content and style loss for the first 3000 updates as shown in the left plot of [@fig:increased-initial-content-loss]. The concatenation method had a five to ten times higher style loss than content loss for the first few hundred iterations. The controlled approach improves this to less than factor 2 and pastiches for the first few thousand iterations only show the content image. Looking at up to 60k parameter updates in the right plot we can see that the content loss is clearly better than for the concatenation approach, but the style loss is worse.


In the left plot of [@fig:punish-vs-naive-naive-has-better-losses] we can see that the style loss gets slightly worse when we force the network to deviate from the style image (penalize blend). The content loss is slightly better, so the overall loss is about the same. The network is learning to deviate from the style image while the blend loss $\el_b$ improves with more parameter updates. The right plot shows us that additionally controlling the content loss improves the blend loss even further. This is intuitive as a pastiche closer to the content has a higher pixel wise difference to the style image.

The combined approach has the same characteristic plot as the control approach shown in [@fig:controlled-vs-combined]. Both losses get worse compared to the control approach by additionally encouraging deviation from the style image.

The network is encouraged to deviate from the style image by the penalize approach, which leads to a worse style and content loss of the combined approach compared to the controlled approach as shown in [@fig:controlled-vs-combined].

<!--  ---------------------------------------  -->
\subparagraph*{Discussion}\nop{*}
Both individual components have visible advantages over the naive concatenation method and worse losses. The pastiches from the penalizing approach often suffer from weak styling and contain visible artifacts. The pastiches from the control approach are very close to the style image in the background of the content image. The combined approach has the advantage of both components. We think that with 40k iterations the results already show the strength of the combined approach and it allows direct comparison to the single-style network baseline that is also trained with 40k iterations. The method clearly solves the *style copy artifacts* and *style blend* issues, so we consider this approach viable for ten-styles.


<!-- Penalize enables style transfer instead of weak styles -->

   <!-- - (However, for some styles (Starry Night, Udnie) the actual style transfer seems to be closer to the single-style result. -->
<!-- content loss control prevents artifacts, but tends to resemble style image where no content -->

  <!-- - We can still see that the pastiche is very close to the style image in the background of the content image. -->
  <!-- - The content is respected (shows the artifact overview) (maybe make a one image series for this) -->






\begin{figure}[h]
 \centering
 \includegraphics[height=0.26\textheight]{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/plot_Loss_mean_on_the_validation_set.pdf}
 \includegraphics[height=0.26\textheight]{source/results/plots_TenStyles/controlledVSconcatenation/plot_Loss_mean_on_the_validation_set_average.pdf}
 \caption[Losses for the controlled content approach vs the concatenation approach]{
   Results from the naive concatenation approach and control content loss approach for the \dd*Ten-Styles Dataset*.
   Controlling the initial content loss to be high ($\lc = 12$) stops the style loss from dominating the content loss in the beginning (left). The right plot shows more iterations. This results in a better content loss than the concatenation method and a worse style loss.
 }    
 \label{fig:increased-initial-content-loss}
\end{figure}


\begin{figure}[h]
  \centering
  \includegraphics[height=0.26\textheight]{source/results/plots_TenStyles/naiveVSpunish/plot_Loss_mean_on_the_validation_set_average.pdf}  
  \includegraphics[height=0.26\textheight]{source/results/plots_TenStyles/penalizedVScombined_blend/plot_Reference_loss_mean_on_the_validation_set_average.pdf}  

  \caption[Losses of the concatenation approach compared to single-style baseline]{       
    Comparison of the \dd*penalized-ten-styles network* and the \dd*concatenation-ten-styles network* (both left).
    The style loss gets slightly worse as we force the network to deviate from the style image. The content loss is slightly better and is able to compensate, so the overall loss is about the same. The network is learning to deviate from the style image as the blend loss $\el_b$ also improves with more parameter updates.    
    Combining the penalized approach with the control approach improves the blend loss (right plot) which agrees with the visual inspection.
  }  
  \label{fig:punish-vs-naive-naive-has-better-losses}
\end{figure}


<!-- \begin{figure}[h]
  \centering
  \includegraphics[height=0.24\textheight]{source/results/plots_TenStyles/controlledVScombined/plot_Loss_mean_on_the_validation_set_average.pdf}  

  \caption[Losses of the Concatenation Approach compared to Single-style Baseline]{       
    The combined methods have the same characteristic curves as the control approach, but content and style loss are worse.
  }  
  \label{fig:controlled-vs-combined}
\end{figure}

 -->

\begin{figure}[h]  
  \centering
  \begin{minipage}[c]{0.48\textwidth}
    \includegraphics[width=\textwidth]{source/results/plots_TenStyles/controlledVScombined/plot_Loss_mean_on_the_validation_set_average.pdf}
  \end{minipage}
  \quad
  \begin{minipage}[c]{0.44\textwidth}
    \captionsetup{singlelinecheck=false,format=plain,justification=raggedright,labelsep=newline}
    \caption[Losses of the combined approach compared to the controlled approach]{
        The network is encouraged to deviate from the style image by the penalize approach, which leads to a worse style and content loss of the combined approach compared to the controlled approach.
    }
    \label{fig:controlled-vs-combined}
  \end{minipage}
\end{figure}













\cleardoublepage
