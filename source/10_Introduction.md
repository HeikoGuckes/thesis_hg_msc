# Introduction

\begin{figure}[b]
\centering
  \newcommand{\thiswidth}{3.2cm}

  \newcommand{\imgcap}[2]{%
    \begin{minipage}{\thiswidth}%
      \includegraphics[width=\thiswidth,height=\thiswidth]{#1}%
      \vspace{-0.2cm}
      \caption*{#2}%
    \end{minipage}%  
  }

  \imgcap{source/images/styles/starry_night.png}{
    Style Image}
  \begin{minipage}{0.8cm}
    \centering
    \Huge{+}
    \large{\quad}
  \end{minipage}
  \imgcap{source/images/content/lena_color.png}{
    Content Image}
  \begin{minipage}{0.8cm}
    \centering
    \Huge{=}
    \small{\quad}
  \end{minipage}
  \imgcap{source/results/Iterative/lena_colorXstarry_night.png}{
    Pastiche}

  \caption[An example for iterative style transfer from Gatys et al. (2016)]{Style transfer inputs and result of the iterative method from Gatys et al. (2016) The resulting pastiche is close to the content image and similar in style to the style image.}
  \label{fig:style-transfer-example-iterative}
\end{figure}

People admire pictures from famous artists in museums all over the world. So, would it not be great to make own personal images appear in the style of the admired artists? We think that many people would agree. Transferring an artist's painting style to a personal image (content image) is a challenging and time consuming task, even for an experienced artist. To achieve this style transfer the artist has to balance (1) the recognizability of important parts within the content image, like a face or object, and (2) the elements in the desired art (style image) that make it unique, e.g. strokes, structures, colors. The resulting styled image (i.e. pastiche) is then close to the content image and similar to the style image (see [@fig:style-transfer-example-iterative]).

To automate this complex and time consuming task different computerized methods have been developed since the nineties [@taxonomy]. Many of those look at the pixelwise difference to achieve a similar output to the style image at a global scale or for small patches [@taxonomy]. The methods have been constantly improved and achieve visually pleasing results, but they all suffer from a fundamental drawback: they do not have a concept of what is important content in an image, so a human has to guide the algorithm, if certain details have to be preserved. For instance, the eyes, nose and mouth of a person need to be preserved, as they are key features to recognize a face. To reduce the necessity for human guidance people used  different machine learning techniques, e.g.\ segmentation.

Nowadays, neural networks are used in almost every area of computer vision and are able to detect objets in images reliably [@vgg2014]. Consequently,  @gatys made use of such a neural network to define a loss function that can judge whether an image is close to a content image and similar in style to the style image. He uses it to *iteratively* optimize a randomly initialized image until a pastiche emerges. This produced excellent results, as in [@fig:style-transfer-example-iterative]. The major drawback is that the method is not fast enough for realtime video rendering or mobile applications. Others tackled this challenge by approximating the result by training a different neural network that directly produces the pastiche in one pass [@Johnson;@Ulyanov]. The result's quality is close to the iterative method and is generated so fast that realtime video and mobile applications become feasible (a few seconds per image on a modern phone). The high quality of the resulting pastiches is the reason why we chose to research the area. These fast methods require the training of one neural network per style (single-style) and consequently lack the flexibility to produce different styles.

The goal of this thesis is to extend the capability of the single-style network from processing only one style to process multiple styles (multi-style). If trained with sufficient styles a network might even be able to generalize to new styles. This would allow people to experiment with arbitrary styles and would save storage space on mobile devices as only a single network would have to be stored. To assess the multi-style challenge we explored the behavior and limitations of the modified single-style network architecture of @Johnson extended with a style input by naive concatenation with the content image.

This *concatenation* network, when trained with ten styles of known artists, *copies artifacts* from the style image into the pastiches and tends to *blend* the style and content image together instead of transferring the style. This behavior becomes even stronger when the network is trained using 80k style images. To overcome these challenges we tried three different approaches.

The first approach *combines* two ideas to address the two challenges directly.
It controls the initial influence of the content image and encourages deviation from the style image.
The second approach is using trained single-style networks to *supervise* the learning of the multi-style network in addition to the normal loss function.
The third approach tries to use a low dimensional style image as input to the network. The idea behind this approach is that the network can not blend the style image if it does not have direct access to it. This approach was not able to produce a functional multi-style network.

The *combined approach* shows comparable quality to the single-style networks, trains ten styles as fast as the single-style network trains a single style, and pastiches are generated as fast. As it punishes equality to the style image, some elements with high contrast from the style image might appear within the pastiche with different colors. The effect was so subtle for the ten styles we used that it did not negatively impact our own perceived quality.

The *supervised approach* also delivers comparable quality, but close inspection reveals that the pastiches are slightly less sharp than those of the single-style networks. It requires more training time to achieve this, but the training of the necessary single-style networks dominates the training time.

When learned with 32 styles, the quality of the *combined approach* is still very high, but it starts to degenerate at 100 styles and regresses to color transfer with more than 200 styles. The networks allow the input of any style, which allows experimentation with untrained styles, but we found that the resulting pastiches are not close to those from the single-style networks.

In summary, we achieved a multi-style network that is capable of transferring ten different styles without sacrificing quality, training time or speed. This could be used to reduce the amount of storage space required on a mobile device. Additionally, styles can be switched without loading a new network. The network allows experimentation with untrained styles, but generalizability is low when trained with random styles.

\subparagraph*{Thesis Structure}\nop{*} After the introduction we continue with [@Sec:theory] about the theory necessary to understand the building blocks of a neural network and how it is trained. [@Sec:related-work] covers related work and puts this thesis into context. [@Sec:limitations] describes our evaluation of the current single-style architecture for multi-style in form of the concatenation network. In this section we also introduce the methods to quantify the quality of the results. In [@Sec:approaches] we present the three different approaches to overcome the limitations of the concatenation network. In [@Sec:results] we present our results and compare them to those of the single-style networks and conclude the thesis and give an outlook in [@Sec:conclusion].
