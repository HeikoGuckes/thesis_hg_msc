# Novel Approaches {#sec:approaches}

We analyzed the single-style architecture extended by a concatenation layer in the last section. The primary limitations or problems for the concatenation approach are the tendency to simply blend the style and content image together (*style-blend*) and to copy big parts of the style image over the content image (*style-copy artifacts*).

We approached this problem in three different ways:

- A combined approach in [@Sec:combined-approach] consisting of two components:
      - Penalize the blending of the style image directly by extending the loss function. This tries to encourage deviation from the style image.

      - Start with a higher content loss and gradually decrease it to prevent the style loss to dominate the loss function for the first few updates. This should stop the network from irreversible learning to copy the style in the beginning, which lead to persistent artifacts that ignored the content.   

- Do not input the style image into the network directly and use a low dimensional version instead in [@Sec:pca-approach]. If the network does not have access to the style image, it has to come up with something different.

- Supervise the learning of the multi-style network by using the output of multiple single-style networks in [@Sec:supervision-approach]. If we show the network the output we desire, it should deviate from the style image.
