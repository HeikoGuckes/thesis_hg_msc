\renewcommand{\el}{\mathcal{L}}
\renewcommand{\els}{\el_{s}}
\renewcommand{\elc}{\el_{c}}
\renewcommand{\ls}{\lambda_{s}}
\renewcommand{\lc}{\lambda_{c}}
\renewcommand{\contentlayers}{C}
\renewcommand{\stylelayers}{S}
\renewcommand{\lb}{\lambda_{b}}
\renewcommand{\lref}{\lambda_{ref}}

\newpage

## Supervised Training of a Multi-Style Network {#sec:supervision-approach}

\begin{figure}
  % **Fig:ReferenceNet**
  \centering
  \includegraphics[width=0.8\textwidth]{source/architecture/ReferenceNetV2.pdf}
  \caption[System overview for training the supervised multi-style network]{
    System overview for training the \dd*supervised-multi-style network*. The loss function is augmented by a perpixel loss between the desired output from the single-style networks and the current output.
  }
  \label{fig:ReferenceNet}
\end{figure}

<!-- \subparagraph*{Reference Network Architecture}\nop{*} -->

The idea of this method is to let one or more single-style networks supervise the training of the multi-style network. To show the \dd*reference-multi-style* network, with to be trained parameters $W_s$, the output we desire, we use our single-style baseline network trained for style $s$ and input $c$ with trained parameters $W_s$ and output $y_{ref}=f_{W_s}(c)$. We introduce the *reference loss* that penalizes deviation from the reference output $y_{ref}$ using the per-pixel loss:

$$
\el_{ref}(p)
=
\frac{1}{N}
\Vert p - f_{W_s}(c)\Vert_{2}^{2}
$$ {#eq:content-loss}

where $N$ is just a normalizing factor representing the number of elements in p.
With this extension the loss function [@Eq:loss-function] becomes:
$$
\el(s, c, p) =  \ls\els(p) + \lc\elc(p) + \lref\el_{ref}(p)
$$ {#eq:loss-function+ref}

where $\lref$ is a scalar. The system overview is shown in [@Fig:ReferenceNet]. During training we feed the content image $c$ into both transformation networks and the loss network. The style image $s$ is only fed into the to be trained multi-style network and the loss network, but it was used previously to train the single-style network. Of course, we always have to load the corresponding parameters $W_s$ during training.

After the training we do not need the reference and loss network any longer and can directly use the multi-style network $f_W(c, s)$ to generate an output pastiche.



<!-- \subparagraph*{Methodology}\nop{*} Methodology
- dual input network
- Gatys loss function + reference loss

- try to copy a network just by looking at the output
- dual input network
- gatys loss function + reference loss
- dual input network
- Gatys loss function + reference loss -->
