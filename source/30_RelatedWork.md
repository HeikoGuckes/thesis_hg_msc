\newpage

# Related Work {#sec:related-work}

We start with a brief overview of former style transfer methods in [@Sec:history]. This allows us to understand the advantages of using neural network based style transfer in [@Sec:style-transfer-with-nn], which we present in detail as this work directly builds upon those proposals.

## Former Style Transfer Methods {#sec:history}

Artistic style transfer is a technique that tries to find a pastiche which is similar in style to the style image $s$ while being close to the content of the content image $c$. The following three paragraphs summarize a few key techniques that we extracted from the insightful taxonomy from @taxonomy.

Early style transfer methods combined virtual brush strokes with local edge detectors like the Sobel operator. Those methods could not comprehend an image on a higher level. They worked on pixels instead but were fast enough for realtime applications.

A major improvement was to look not only at the image at one scale but at multiple scales, starting to draw with bigger virtual brushes for the coarsest scale and going down to smaller brushes for the finer details. In areas with no details gradients are weak and thus the larger strokes are still visible.
Machine learning techniques like segmentation enabled styles like cartoon flat shading.
Manually specified masks were used to preserve perceptually important detail, which makes it obvious that actual knowledge about the semantic of the content is important.

Later techniques added global optimization: An initial image is drawn with strokes guided by the former mentioned methods. In a second step all of the strokes are moved, deleted or added within the image to minimize the distance to the style image measured by an objective function. The function captured things like the pixel wise difference, number of strokes, area of strokes and the coverage of the canvas.

Machine learning techniques used training pairs of content images and their corresponding pastiches to learn a transformation function to create new pastiches for unlearned content images [@Hertzmann:2001]. This is achieved by splitting the content image for the desired pastiche into small patches, which are just small areas in an image. The next step is to find a patch in a database of content images that is statistically close to the desired patch. A simple statistic could be the pixelwise distance. Finally the patch at the same position in the corresponding existing pastiche is copied into the desired pastiche. This is done for all patches in an image to get a complete pastiche.

Similar approaches have been used for texture synthesis [@Efros:2001]. Here is the goal to generate multiple similar looking textures for which a human is not capable of identifying the original texture.

Another method for texture synthesis is to use a *parametric texture model*. It uses set of statistical measurements for a source texture and considers all textures equal that fulfill the same statistics. This idea originates from @Julesz1962, who used Nth-order joint histograms of a textures pixel to describe them. A step further, the statistics were not calculated directly from the image pixels, but on filter responses like oriented band-pass filters inspired by a model of human texture perception [@Heeger:1995] or joint statistics of complex wavelet coefficients [@Portilla2000].

The idea to use a neural network that was pretrained for image classification as a *parametric texture model* led to very high texture quality without the need for a complex handcrafted statistical model [@Gatys2015]. To bridge this idea to style transfer the output needs to be additionally constrained to be close to the content image.

<!-- artistic rendering primitives (regions, strokes, stipples, tiles)[@taxonomy] -->

<!-- - Task can be seen as Texture synthesis
- they only use low level image features (pixel based loss)
- you want a system that is able to detect the content so you can apply style
- Deep Convolutional Neural Networks are able to extract high level features
- gatys uses optimisation to solve this -->



<!-- ## Image Style Transfer Using Convolutional Neural Networks -->


<!-- ###   Gatys - Image Style Transfer Using Convolutional Neural Networks
  - what, advantages, disadvantages
basic idea, used architecture, image of architecture, iterative optimizing, vgg16 vs 19, image of our gatys implementation results -->
