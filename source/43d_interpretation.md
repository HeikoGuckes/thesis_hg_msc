<!--  ---------------------------------------  -->
\subparagraph*{Interpretation}\nop{*}

We reached our goal in getting the *reversed approach* single-content network output pastiche closer to the content-image by adding the content-image as an input to the network. This shows that the content image is important for a clear content representation within the pastiche for some styles. We even reached better losses than the single-style baseline. We attribute this to the fact that it only has to learn one content image and ten style images. Unfortunately, this also tells us that adding the style image to the network makes it easier for the network to get the output closer to the style image, but we desire the output to be similar and not close or equal.

This is exactly the problem that surfaces when we increase the amount of content images for the network to learn. We get *style copy artifacts* or *style blending* instead of style transfer. We saw that the style-elements pop up after only one thousand iterations. This is the same range where the style loss is dominating the total loss. We conclude that in the training process, the network learns to copy or pass the style image to the output and learns to ignore the content image in the first few hundred iterations and when the content loss starts to become a significant part of the loss it starts to incorporate the content, but it seems not strong enough or too late to displace the style and correct the initial mistake. This leads to the artifacts from the *ten-styles-concatenation network* in every pastiche that the network can not unlearn stemming from the style image.
<!-- This also leads to the style image dominating in the blended images for the *painters-concatenation network* where the content image is barely visible. -->

We need to stop the network from copying and blending the style image. This is not an easy task as the style copy is a very good strategy. We conclude this from the observation that the *ten-styles-concatenation network* reaches lower style losses, despite the fact that it has to generalize over ten styles whereas each *single-style network* is optimized for a single style.

If we train a single-style network without the content loss, we can create textures that fulfill the same statistic as the style-image [@Gatys2015]. If we do the same with the multi-style network, which has direct access to the style image, it could simply output the style image and the loss would be zero. We conclude that for the concatenation networks the only incentive to deviate from the style image is the content loss. This is a big challenge that we try to overcome in the following sections by giving the network incentives to deviate from the style image.


 <!-- Folien: We conclude that for the concatenation networks the only incentive to deviate from the style image is the content loss. This is a big challenge.  -->


<!-- One question forces itself upon us: Why does style blending reach such a good local minimum for our style loss function?
If we think back to our introduction of a *parametric texture model*:
Use a set of statistical measurements for a source texture and consider all textures equal that fulfill the same statistics. Of course, the style image, which is the source texture for our texture model, fulfills the statistic perfectly. In other words, the style loss is zero for the style image. Of course our loss function has two components and without the content loss, the network would not even have an incentive at all to produce anything different than the style image itself and it has direct access to it. -->
