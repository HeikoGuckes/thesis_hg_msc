\newpage

--------------------------------------------------------------------------------------------------
Operation                                   Kernel size  Stride Feature maps Padding  Nonlinearity
---------------------------------           -----------  ------ ------------ -------  ------------
**Network** - 256 x 256 x 3

\ Reflect Padding with width 54

\ Convolution                               9            1      32           VALID    ReLU

\ Convolution                               3            2      64           VALID    ReLU

\ Convolution                               3            2      128          VALID    ReLU

\ Residual block                                                128

\ Residual block                                                128

\ Residual block                                                128

\ Residual block                                                128

\ Residual block                                                128

\ Upsampling                                                    64

\ Upsampling                                                    32

\ Convolution                               9            1      3            VALID    tanh

\ Scale the tanh with constant (150)

\ Crop input by 1 due to VALID

\

**Residual block** - C feature maps

\ Convolution                               3            1      C            VALID    ReLU

\ Convolution                               3            1      C            VALID    Linear

\ Crop input by 2 due to VALID

\ Add the input and the output

\

**Upsampling** - C feature maps

\ Nearest-neighbor interpolation

\ with factor 2

\ Convolution                               3            1      C            VALID    ReLU
---------------------------------           -----------  ------ ------------ -------  ------------

Table: Single-style network architecture components \label{tbl:single-style-architecture}



------------------------------------------------------------------------------------------------------------
**Parameters**
---                                         -------------------------------------
\ Normalization                             Instance normalization after every convolution

\ Optimizer                                 Adam [@adam] ($\alpha = 0.001,  \beta_1 = 0.9,  \beta_2 = 0.999$)

\ Parameter updates                         40000

\ Batch size                                4

\ Weight initialization                     Isotropic gaussian ($\mu = 0, \sigma = 0.01$)

-----------------------------------------------------------------------------
Table: Single-style network training parameters \label{tbl:single-style-architecture-parameters}
