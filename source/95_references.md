\newpage

\footnotesize

<!--
Do not edit this page.

References are automatically generated from the BibTex file (References.bib)

...which you should create using your reference manager.
-->



<!-- WORKAROUND TO ALLOW PAGE BREAKS IN BIBLIOGRAPHY -->
<!-- source: https://groups.google.com/forum/#!topic/pandoc-discuss/IR7nL7lEHpc  -->

\let\oldhypertarget\hypertarget

\renewcommand{\hypertarget}[2]{%
    \penalty 0%
    \oldhypertarget{#1}{#2}%
}

<div id=“refs”></div>
<!--  END OF WORKAROUND  -->


# References {.allowframebreaks}
