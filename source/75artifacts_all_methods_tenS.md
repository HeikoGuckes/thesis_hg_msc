
<!-- ### Style Copy Artifacts -->

\subparagraph*{Artifacts}\nop{*}

All of our approaches reduce the style-copy artifacts over the concatenation approach as shown in [@tbl:artifacts-ten-styles-all-methods]. The concatenation baseline copies about 7.3% similar pixels $(\pm 3)$ into every pastiche, measured per color channel for all ten styles over three content images. As the penalize style blending approach tries to encourage deviation from the style image and does not directly address the artifacts, it shows the lowest improvement but still brings the similar pixels down to 4.9%. Controlling the content loss directly addresses the problem and reduces the value to 2.4%. It has the problem that the background, where no content object is present, often resembles the style image. The combination of controlling and penalizing gets rid of that problem and most artifacts. It consequently reduces the value further to 1.6%. The supervision approach requires 80k parameter updates to surpass that value and reaches 1.1%. It even benefits from more iterations and reaches 0.9% at 120k updates, but some initial spot-like artifacts remain. Interestingly the single-style networks sometimes produce white spot artifacts too, but they are usually not in the same place for every content image. Training again with the same parameters can sometimes solve this, as the network is initialized with different weights. The single-style baseline and the iterative method produce around 0.5% similar pixels in every pastiche.   
When looking at the extracted artifacts in [@fig:artifacts-ten-styles-all-methods] the improvements are clearly visible. It shows that some styles have a tendency to have the same colors at the same position. Black spots indicate positions where all three colors are equal and they are nearly completely alleviated by the 120k supervision and combined approach.



Pastiche from                    Similar pixels
----------------------   ----------------------
\naivenet                                  7.3%
\penalizednet                              4.9%
\controllednet                             2.4%
\combinednet                               1.6%
\supervisednet 40k                         2.0%
\supervisednet 80k                         1.1%
\supervisednet 120k                        0.9%
\singlenet                                 0.5%
\Siterative                                0.5%
-----------------------------------------------
Table: Similar pixels $(\pm 3)$ over 10 different content images. {#tbl:artifacts-ten-styles-all-methods}








\renewcommand{\inclimg}[2][]{%
  \frame{%
    \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
        {#2}%
      }%
        \\ \vspace{-0.875cm}%
        \\ %

}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%
  \inclimg{#1#2composition_vii.png}%
  \inclimg{#1#2famosas-crop.png}%
  \inclimg{#1#2wave.png}%
  \inclimg{#1#2feathers.png}%
  \inclimg{#1#2la_muse.png}%
  \inclimg{#1#2mosaic.png}%
  \inclimg{#1#2starry_night.png}%
  \inclimg{#1#2the_scream.png}%
  \inclimg{#1#2udnie.png}%
}


\renewcommand{\mpwidth}{1.65cm}
\renewcommand{\mphspace}{\hspace{-0cm}}




\begin{figure}[htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}

  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}
  \newcommand{\iterative}{source/results/iterative/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\combined}{source/results/combined/combine_punish_lc12to1_TenStyles40k/}  
  \newcommand{\linlc}{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/}
  \newcommand{\reference}{source/results/supervision/InstanceMultiStyle40k/}
  \newcommand{\referenceEighty}{source/results/supervision/InstanceMultiStyle80k/}
  \newcommand{\referenceOneTwenty}{source/results/supervision/InstanceMultiStyle120k/}

  %\newcommand{\single}{source/results/InstanceSingleStyle40k/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.4cm}%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}%
      }%    
  }


  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\istyles}{}
    \captionv{Style image}
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\iterative}{artifactsX}
    \captionv{Iterative}
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\single}{artifactsX}
    \captionv{Single-style}
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\referenceOneTwenty}{artifactsX}
    \captionv{Superv. 120k}
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\referenceEighty}{artifactsX}
    \captionv{Superv. 80k}
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\reference}{artifactsX}
    \captionv{Superv. 40k}
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\combined}{artifactsX}
    \captionv{Combined}
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\linlc}{artifactsX}
    \captionv{$\lc$-control}
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\punish}{artifactsX}
    \captionv{Penalize}
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\naive}{artifactsX}
    \captionv{Concat.}
	\end{minipage}

  \caption[Extracted artifacts due to invariance for all approaches for the Ten-Styles Dataset]{
    Visualization of the style-copy artifacts due to invariance for all baselines and all approaches on the \dd*Ten-Styles Dataset*.
    The \textit{penalize} style blending approach is a slight improvement over the naive \textit{concat}enation.
    \textit{Controlling} the influence of the content image reduces the \dd*style copy artifacts* problem substantially. \textit{Combined}, those two approaches achieve near zero similar pixels for some styles.
    \textit{Supervising} the multi-style network by using the single-style networks as a reference reaches the level of the \textit{single-style} networks at 80k parameters updates aside from a few persistent spots. The \textit{iterative} approach is about equal to the single-style networks.
    }

  \label{fig:artifacts-ten-styles-all-methods}
\end{figure}



<!-- \cleardoublepage -->
