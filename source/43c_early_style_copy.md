\begin{figure}
\centering
  \newcommand{\thiswidth}{2.8cm}

  \newcommand{\imgcap}[2]{%
    \begin{minipage}{\thiswidth}%
      \includegraphics[width=\thiswidth,height=\thiswidth]{#1}%
      \vspace{-0.2cm}
      \caption*{#2}%
    \end{minipage}%  
  }

  \newcommand{\naiveFourty}{source/results/naive/naiveCocoXTenStyles40k/}

  \imgcap{source/images/styles/candy.png}{          Candy Style}
  \imgcap{source/images/content/chicago.png}{       Chicago Content}
  \imgcap{\naiveFourty chicagoXcandy001000.png}{    1k Updates}
  %\imgcap{\naiveFourty chicagoXcandy002000.png}{    2k Updates}
  %\imgcap{\naiveFourty chicagoXcandy003000.png}{    3k Updates}
  %\imgcap{\naiveFourty chicagoXcandy004000.png}{    4k Updates}
  \imgcap{\naiveFourty chicagoXcandy005000.png}{    5k Updates}
  \imgcap{\naiveFourty chicagoXcandy.png}{          40k Updates}

  \caption[Visualization of the origin of the style-copy problem]{
     \dd*Ten-styles-concatenation network* results visualizing the origin of the \dd*style copy artifacts* problem. The style artifacts are introduced very early during training due to the high initial style loss.
  }
  \label{fig:concatenation-ten-styles-style-copy-origin}
\end{figure}
