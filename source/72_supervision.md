
## Evaluation of the Supervision Approach for Ten Style

The idea for this approach is to use the output of each respective single-style network as a reference for training the multi-style network. In addition to this supervision idea, the style and content loss summands of the loss function are still contributing ($\ls=5,\lc=1$).
This section answers (1) the question of general viability for multi-style and (2) if the style and content loss are relevant at all when we have the single-style reference available and (3) how many parameter updates the training of such a multi-style network requires to deliver viable results.

\subparagraph*{Visual}\nop{*} A network learned solely from the output of the \dd*single-style networks* ($\ls=0,\lc=0,\lref=100$) produces a washed out or blurry pastiche. Adding the style loss ($\ls=5$) from the perceptual loss function to sharpen the result without the content loss ($\lc=0$) leads to \dd*style copy artifacts* ignoring the content. The \dd*supervision-ten-style network* is using all parts of the loss function ($\ls=5,\lc=1,\lref=100$) and leads to a sharper pastiche with minimal artifacts as shown in [@fig:supervision-different-params-at-zero]. We summarized the results from different parameters in [@tbl:multi-style-hyper-parameter-impact]. All networks in this section have been learned with 120k parameter updates to show that a lot more updates do not alleviate the blurriness completely.

 \begin{figure}[h]
 \centering
   \newcommand{\thiswidth}{3.2cm}

   \newcommand{\imgcap}[2]{%
     \begin{minipage}{3.35cm}%
       \centering%
       \includegraphics[width=\thiswidth,height=\thiswidth]{#2}%
       \vspace{-0.2cm}%
       \caption*{\begin{scriptsize}#1\end{scriptsize}}%  
     \end{minipage}}

   \imgcap{Content Image  \\ \ }{source/images/content/chicago.png}
   \imgcap{Style Image  \\ \ }{source/images/styles/candy.png}
   \imgcap{Reference (Ref.) Loss Only \\ $\ls=0,\lc=0,\lref=100$}{source/results/supervision/b4_ls0_lc0_lref100/chicagoXcandy120000.png}%unsharp
   \imgcap{ Ref. and Style Loss  \\ $\ls=5,\lc=0,\lref=100$}{source/results/supervision/b4_ls5_lc0_lref100/chicagoXcandy080000.png}%artifacts
   %\imgcap{$\ls=0,\lc=1,\lref=1000$}{source/results/supervision/b4_ls0_lc1_lref1000/chicagoXcandy120000.png}%unsharp
   \imgcap{Ref., Style, and Content Loss\\ $\ls=5,\lc=1,\lref=100$}{source/results/supervision/InstanceMultiStyle120k/chicagoXcandy.png}%multi-style

   \caption[The supervision approach without the style and content loss produces a blurry pastiche]{
     The supervision approach without the style and content loss produces a blurry pastiche.
   }
   \label{fig:supervision-different-params-at-zero}
 \end{figure}



 ---------------------------------------------------------------------------------------------
 $\ls$            $\lc$              $\lref$                 Result
 ---              ---                ---                     ---------------------------------------------------------------------------                  
 5                1                  0                       the concatenation method

 0                1                  0                       outputs the content image

 1                0                  0                       outputs the style image

 0                0                  1                       blurry pastiche

 5                0                  100                     sharper, but introduces style copy artifacts

 5                1                  100                     sharper results, minimal artifacts
 ---------------------------------------------------------------------------------------------

 Table: *Supervision-ten-style network* parameter impact. {#tbl:multi-style-hyper-parameter-impact}


[@fig:visual-supervision-approach-two-rows] shows that the pastiches produced by the \dd*supervision-ten-style network* improve in sharpness when the number of parameter updates is increased. We can also see that the sky and the skyline are both styled more intensely compared to the concatenation approach, e.g.\ the walls of the towers are not straight but warped.




\renewcommand{\mpwidth}{2.70cm}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%
  %\inclimg{#1#2composition_vii.png}%
  %\inclimg{#1#2famosas-crop.png}%
  %\inclimg{#1#2wave.png}%
  %\inclimg{#1#2feathers.png}%
  \inclimg{#1#2la_muse.png}%
  %\inclimg{#1#2mosaic.png}%
  %\inclimg{#1#2starry_night.png}%
  %\inclimg{#1#2the_scream.png}%
  %\inclimg{#1#2udnie.png}%
}

\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.275cm}%
      \\}

\begin{figure}[htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\combined}{source/results/combined/combine_punish_lc12to1_TenStyles40k/}  
  \newcommand{\linlc}{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/}
  \newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}  
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\multiFourty}{source/results/supervision/InstanceMultiStyle40k/}  
  \newcommand{\multiEighty}{source/results/supervision/InstanceMultiStyle80k/}
  \newcommand{\multiOneTwenty}{source/results/supervision/InstanceMultiStyle120k/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.8cm}%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}}}

  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\istyles}{}
    \captionv{Style image}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \inclimg{source/images/content/chicago.png}
    \captionv{Content image}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\multiOneTwenty}{chicagoX}
    \captionv{Supervision 120k}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\multiEighty}{chicagoX}
    \captionv{Supervision 80k}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\multiFourty}{chicagoX}
    \captionv{Supervision 40k}  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\naive}{chicagoX}
    \captionv{Concatenation}  
  \end{minipage}
  \hfill

  \caption[Visual comparison of the supervision approach with the single-style networks for ten styles]{  
   More parameter updates for the supervision approach result in sharper pastiches.
  }

  \label{fig:visual-supervision-approach-two-rows}
\end{figure}





<!--  ---------------------------------------  -->
\subparagraph*{Artifacts}\nop{*}
In [@fig:images-combined-VS-concat-artifacts] we can see that the supervision approach greatly reduces the *style copy artifacts* for the *Candy* style. Except for one persistent spot that is also visible in the mean pastiche over three content images, the amount of near equal pixels is close to zero after 120k parameter updates. More parameter updates help reducing the artifacts.


\renewcommand{\mpwidth}{2.70cm}
\renewcommand{\mphspace}{\hspace{-0cm}}

\renewcommand{\inclimgNF}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.875cm}%
      \\ %                
}

\renewcommand{\inclimg}[2][]{%
  \frame{%
    \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
        {#2}%
      }%
        \\ \vspace{-0.875cm}%
        \\ %

}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2composition_vii.png}%  
}

\renewcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%
}

\begin{figure}[htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}
  \newcommand{\combined}{source/results/combined/combine_punish_lc12to1_TenStyles40k/}  
  \newcommand{\linlc}{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/}
  \newcommand{\reference}{source/results/supervision/InstanceMultiStyle40k/}
  \newcommand{\referenceEighty}{source/results/supervision/InstanceMultiStyle80k/}
  \newcommand{\referenceOneTwenty}{source/results/supervision/InstanceMultiStyle120k/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.26cm}%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}}}

  \begin{minipage}{\mpwidth}
    \inclimgNF{source/images/styles/candy.png}
    \captionv{Candy Style}  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\referenceOneTwenty}{meanX}
    \captionv{Mean Superv. 120k}
	\end{minipage}  
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\referenceOneTwenty}{artifactsX}
    \captionv{Supervision 120k}
	\end{minipage}  
  \hfill
  \hspace{0.015cm}
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\referenceEighty}{artifactsX}
    \captionv{Supervision 80k}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\reference}{artifactsX}
    \captionv{Supervision 40k}   
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\naive}{artifactsX}
    \captionv{Concatenation}  
	\end{minipage}
  \hfill

  \caption[Artifacts comparison for different iterations of the supervision approach]{      
      Artifacts comparison for different iterations of the supervision approach
    }

  \label{fig:images-supervision-VS-concat-artifacts}
\end{figure}


\subparagraph*{Losses}\nop{*}
The supervision approach clearly has a worse style loss than the concatenation approach as shown in [@Fig:supervision-vs-concatenation-ls-lc-lb]. While it is hard to see if the concatenation approach improves with more parameter updates after 40k, the style loss and the blend loss for the supervision approach clearly improve even after 80k updates. This agrees with our visual inspection that the supervision approach benefits from more training time.

\begin{figure}  
  \begin{minipage}[c]{0.46\textwidth}
    \includegraphics[width=\textwidth]{source/results/plots_TenStyles/supervisionVSnaive/plot_Loss_mean_on_the_validation_set_average.pdf}
  \end{minipage}
  \hfill
  \begin{minipage}[c]{0.49\textwidth}
    \captionsetup{singlelinecheck=false,format=plain,justification=raggedright,labelsep=newline}
    \caption[Loss comparison for the concatenation approach and the supervision approach for the Ten-Styles Dataset]{
      Results from the naive concatenation approach and the supervision approach for the \dd*Ten-Styles Dataset*.
      The supervision approach clearly has a worse style loss than the concatenation approach. Style loss and the blend loss for the supervision approach improve even after 80k updates. We did not train the concatenation networks with more than 80k iterations as there is no benefit.
    }
    \label{fig:supervision-vs-concatenation-ls-lc-lb}
  \end{minipage}
\end{figure}


\subparagraph*{Discussion}\nop{*}
We saw that without the style and content loss the results are less sharp, so all parts of the loss function are necessary. This approach clearly reduces *style copy artifacts* and *style blend* issues of the concatenation approach, so we consider this approach viable for ten styles. The overall training time for this approach is very high anyway, since it requires trained single-style networks in the first place. Additionally, the approach benefits greatly from more parameter updates. Therefore, we decided to use the 120k updates results for our final visual comparison.
