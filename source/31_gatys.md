\renewcommand{\el}{\mathcal{L}}
\newcommand{\els}{\el_{s}}
\newcommand{\elc}{\el_{c}}
\newcommand{\ls}{\lambda_{s}}
\newcommand{\lc}{\lambda_{c}}
\newcommand{\contentlayers}{C}
\newcommand{\stylelayers}{S}

\newpage


## Style Transfer Using Convolutional Neural Networks {#sec:style-transfer-with-nn}

<!--  was origin of fast single style architecture -->
<!-- ### Problem Statement -->

In this section we will have a look at some of the neural network based architectures used to guide the creation of a pastiche. We start with the essential perceptual loss function proposed by @gatys that is used to iteratively optimize a randomly initialized image to create a pastiche. This process is slow, since every iteration requires a forward and backward pass through a convolutional neural network. This is the reason why we look at the architecture from @Johnson that allows us to train a single style feed-forward neural network that can create a pastiche in one forward pass, but requires to train one network per style.
At the end of this section we will have a look at existing methods to overcome this limitation.

<!-- The iterative method gives us a function $f(s,c)$. The mentioned forward method leaves us with a function $f_{W_s}(c)$, where the style $s$ is present within the weights $W_s$ of the single style network.
At first we introduce the loss function, the iterative method and the single style network.
After that we present several architectures that try to extend the single style network training architecture to learn a true multi-style function $f(s,c)$ like the original iterative variant, but as fast as the single-style forward variant. -->


### Perceptual Loss Function

We recall the definition: Artistic style transfer is a technique that tries to find a pastiche which is similar in style to the style image $s$ while being close to the content of the content image $c$. *A Neural Algorithm of Artistic Style* from @gatys gives a new definition for what is close in style and similar in content based on neural network:

- An image is close in content to another image if the euclidean distance of their features is close. The features are high-level features produced by a trained neural network for image classification.

- An image is similar in style to another image if the Frobenius norm of the difference of their features Gram matrices is small. The features are low and high-level features produced by a trained neural network for image classification.

 <!-- Feature Extraction examples for Low-Level algorithms are Edge Detection, Corner Detection etc. -->

<!-- The original *Neural Algorithm of Artistic Style* starts from a random image $p$ and iteratively optimizes $p$ to minimize the -->

This definition leads to the perceptual loss function:

$$
\el(s, c, p) =  \ls\els(p) + \lc\elc(p)
$$ {#eq:loss-function}

where $\els$ is the style loss and $\elc$ is the content loss (also called feature reconstruction loss) and $\ls$ and $\lc$ are parameters which allow us to control the amount of style relative to the content. It uses a neural network trained for image classification, which we call loss network $\phi$. Given the activation of the loss network at layer $j$ is $\phi_j$, the content loss is defined as:

$$
\elc(p)
=\sum_{j \in \contentlayers} \el_{c}^{j}(p)
=\sum_{j \in \contentlayers}
\frac{1}{N_{j}}
\Vert\phi_{j}(p)-\phi_{j}(c)\Vert_{2}^{2}
$$ {#eq:content-loss}

where $\contentlayers$ is a set of selected output layers (content layers) and $N_j$ is a normalizing factor representing the number of elements at layer $j$. The style loss is defined as:

$$
\els(p)
=\sum_{i \in \stylelayers} \el_{s}^{i}(p)
=\sum_{i \in \stylelayers}
\frac{1}{N_{i}}
\Vert  
  G(\phi_{i}(p)) - G(\phi_{i}(s))
\Vert_{F}^{2}
$$ {#eq:style-loss}

where $\stylelayers$ is a set of selected output layers (style layers), $N_i$ is a normalizing factor representing the number of elements at layer $i$ and $G(x)$ is the Gram matrix. Style and content layers are usually different.

The Gram matrix is usually defined as $G(A) = A^T A$ for an arbitrary matrix $A$. Unfortunately, the output layer $\phi_j$ usually has a shape $C_j \times H_j \times W_j$, where $C_j$ is the number of feature maps of height $H_j$ and width $W_j$, so the output is not matrix shaped and we can not compute the Gram matrix directly. To avoid this we can vectorize each feature map to get a matrix shape of $C_j \times H_j W_j$. We call this reshape transformation $r$ and define the Gram matrix as:

$$ G(\phi_j) = r(\phi_j)r(\phi_j)^T$$ {#eq:gram-matrix}

<!-- \frac{1}{C_j H_j W_j}  -->

<!-- $$ G_{j}^{\phi}(x)_{c,c'}=\displaystyle \frac{1}{N_j}\sum_{h=1}^{H_{j}}\sum_{w=1}^{W_{j}}\phi_{j}(x)_{h,w,c}\phi_{j}(x)_{h,w,c'}
$$ {#eq:gram-matrix-long} -->


The style loss is a *parametric texture model* that uses the gram matrix as a statistic on the filter responses (feature maps) of the neural network. In other words, the combination of the gram matrix with the output of the neural network tells us when two images are similar in style. @Johnson give a good intuition of why the Gram matrix is a useful statistic:

<!-- r($\phi_{j}(x)$) gives us $C_{j}$ flattened feature maps. If we look at one entry in the gram matrix it is a combination of two feature maps. -->

<!-- ###   Every Filter Extracta a Specific Texture in Convolutional Neural Networks -->
<!-- "The experimental results support our as- sumption that the style of an image is essentially a combination of texture primitives captured by filters in CNNs. In addition to gen- erate images of diverse styles, we also provide an explanation about why Gram matrix [1] of feature maps could be a representation of image style. Since every filter extracts a specific texture, the com- bination weights of feature maps decide the image style. Like the sum of feature maps along channel axis, Gram matrix also guides the energy of every feature map of generated image." -->

"If we interpret $\phi_{j}(x)$ as giving $C_{j}$-dimensional features for each point on a $H_{j}\times W_{j}$ grid, then $G_{j}^{\phi}(x)$ is proportional to the uncentered covariance of the $C_{j}$-dimensional features, treating each grid location as an independent sample. It thus captures information about which features tend to activate together."

We also want to look at the question: Why is the pretrained neural network a good filter?
To be able to classify images a convolutional neural network had to learn how to compress the perceptual and semantic information from layer to layer. Therefore, the level of abstraction increases when we look at higher level features within a neural network trained for image classification [@Zeiler2014]. Consequently, only a rough approximation can be reconstructed from high-level feature at a deeper layer, which is exactly what we want for the content loss. As for the style loss, we already mentioned in the first part of this section that looking at different scales of an image helps in creating a convincing pastiche. Different layers capture different levels and details of the corresponding style [@Johnson].

\subparagraph*{Total Variation Regularization}

Total variation regularization (tv loss) is added by @Johnson to the loss function to prevent smaller artifacts and smooth out the resulting image. For discrete images $x$ it is approximated with:

$$
\el_{tv}(x) = \sum_{i,j}(  (x_{i,j+1)} - x_{ij})^2   +   (x_{i+1,j)} - x_{ij})^2   )^{\beta/2}
$$ {#eq:loss-function-tv}

with $\beta$ usually in the range of [1, 2] where $\beta = 2$ leads to more washed out images and $\beta = 1$ might produce different artifacts when the tv loss is used to train convolutional neural networks [@Mahendran].


### Style Transfer Using Iterative Image Optimization

\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{source/architecture/LossNet.pdf}%
  \caption[The loss network based on VGG-16]{The loss functions $\els$ and $\elc$ use the loss network (VGG-16) $\phi$ to evaluate the input image $p$. The backpropagated error can be used to optimize p iteratively.}
  \label{fig:loss-network}
\end{figure}

To calculate the loss $\el(s, c, p)$ a loss network $\phi$ has to be chosen. In theory any convolutional neural network pretrained for object classification could be used. While the original work from @gatys uses VGG-19, @Johnson chooses VGG-16 for his experiments. Both variants can be found in the work from @vgg2014. The resulting loss function based on VGG-16 is illustrated in [@Fig:loss-network].

By iteratively optimizing the following equation an output image $\hat y$ is generated:

$$
\hat y
= \arg\min_p \biggl[
     \lc \elc(p)
   + \ls \els(p)
   + \lambda_{tv} \el_{tv}(p) \biggr]
%= \arg\min_p (
%     \lc \sum_{j \in \contentlayers} \el_{c}^{j}(p)
%   + \ls \sum_{i \in \stylelayers} \el_{s}^{i}(p)
%   + \lambda_{tv} \el_{tv}(p) )
$$ {#eq:argmin-loss}

where $\lc, \ls and \lambda_{tv}$ are scalars used to control the relative strength between the different summands. $p$ is initialized randomly and gradually optimized with every iteration. The original optimization method is L-BFGS [@lbfgs]. Adam is also an option [@adam]. Therefore, we consider this an implementation detail. The original iterative method from @gatys does not use any tv loss, but was added by @Johnson to smooth the output and prevent artifacts.
