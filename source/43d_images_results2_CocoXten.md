\renewcommand{\mpwidth}{2.80cm}
\renewcommand{\mphspace}{\hspace{-0cm}}

\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.275cm}%
      \\ %                
}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%  
  \inclimg{#1#2composition_vii.png}%  
  \inclimg{#1#2famosas-crop.png}%
  \inclimg{#1#2wave.png}%
  \inclimg{#1#2feathers.png}%
  %\inclimg{#1#2la_muse.png}%
  %\inclimg{#1#2mosaic.png}%
  %\inclimg{#1#2starry_night.png}%  
  %\inclimg{#1#2the_scream.png}%
  %\inclimg{#1#2udnie.png}%
}

\renewcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  %\inclimg{#1#2candy.png}%  
  %\inclimg{#1#2composition_vii.png}%  
  %\inclimg{#1#2famosas-crop.png}%
  %\inclimg{#1#2wave.png}%
  %\inclimg{#1#2feathers.png}%
  \inclimg{#1#2la_muse.png}%
  \inclimg{#1#2mosaic.png}%
  \inclimg{#1#2starry_night.png}%  
  \inclimg{#1#2the_scream.png}%
  \inclimg{#1#2udnie.png}%
}

\begin{figure}[htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}

  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\punish}{source/results/naive/naiveCocoXTenStyles40k/}
  %\newcommand{\naive}{source/results/punish/PunishBlendVGGTenStyleslb200_20k/}
  %\newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_20k/}
  %\newcommand{\naive}{source/results/InstanceSingleStyle40k/}
  %\newcommand{\punish}{source/results/InstanceSingleStyle40k/}
  %\newcommand{\naive}{source/results/punish/PunishBlendMultiStyle40k/}
  %\newcommand{\punish}{source/results/punish/PunishBlendMultiStyle40k/}



  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\istyles}{}
    \caption*{Style image}  
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \inclTenStyleTwo{\naive}{chicagoX}
    \caption*{Chicago}  
	\end{minipage}  
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/hoovertowernight.png}
    \inclTenStyleTwo{\punish}{lena_colorX}
    \caption*{Hoover Tower}  
	\end{minipage}
  \mphspace
  \hspace{0.015cm}
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\istyles}{}
    \caption*{Style image}  
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \inclTenStyleOne{\naive}{chicagoX}
    \caption*{Chicago}  
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/hoovertowernight.png}
    \inclTenStyleOne{\punish}{lena_colorX}
    \caption*{Hoover Tower}  
	\end{minipage}
  \hfill



  \caption[Visualization of the style-copy problem of the concatenation approach for the Ten-Styles Dataset]{
    Results from the \dd*ten-styles-concatenation network*. For every style from the \dd*Ten-Styles Dataset* we see \dd*style copy artifacts* that occur in all content images. For some styles mostly the color is transferred.
    }

  \label{fig:naive-blending-problem}
\end{figure}
