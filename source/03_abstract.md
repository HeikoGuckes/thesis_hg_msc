<!-- \begin{abstract}\end{abstract} -->
\selectlanguage{english}

<!-- # Abstract {.unnumbered} -->

\section*{Abstract}

Transferring the style of a great artist to an image is a challenging and time consuming task even for an experienced artist. Therefore, a lot of work has been invested to transfer styles automatically. Many of the earlier methods lack a concept of which objects are important in an image. A significant improvement in quality has been achieved by a recently developed method from Gatys et al. by calculating a loss on a neural network to capture the style using a global statistic. This network has been pretrained for object recognition. The method works for every content and style combination (multi-style), but requires minutes of computation, because it *iteratively* optimizes an image regarding the loss. One way to improve the speed is to approximate the loss by moving the computation to training time using a feedforward neural network. They deliver styled images in realtime with similar quality. Unfortunately, one network has to be trained for every single style (single-style).    
Our goal is to create a multi-style feedforward network optimizing the same novel global statistic as the iterative approach. Existing solutions either do not allow the input of unlearned styles or are slower than the single-style network. We explore the limitations of a state of the art single-style network for the multi-style challenge with a naive concatenation of style and content image at its input, so it accepts different content and style images. When trained with ten styles it mainly captures the colors and some artifacts that are inspired by the style image.  
We contribute two different successful approaches trained on the Microsoft COCO Dataset that overcome these limitations for at least ten styles. A third approach, based on dimensionality reduction, was not capable of multi-style transfer. The first approach is using the single-style networks to supervise the learning of the multi-style network, which requires trained single-style networks. The styled images are slightly less sharp than those from the single-style networks. The second approach increases the initial influence of the content loss and discourages pixelwise similarity to the style image at the output of the loss network. For ten styles it trains as fast as a state of the art single-style network, produces images as fast with what we perceive as similar quality, and we measure results within 5% of the loss function.


<!-- Problems:

Wie evaluiert, welcher datensatz, beweiss dass schneller

Buzzwords im titel sollten im abstract und intro vorkommen

Was ist style, content und pastiche.
-> I think it is clear what a style in connection with art is.

-->

<!-- Transferring the style from a great artist to an image with arbitrary content is something many people desire. Creating such a pastiche manually is a challenging and time consuming task even for a talented artist. Therefore, a lot of work has been invested to automatically transfer styles. Many of the earlier methods lack a concept of what is important in an image. A stunning improvement in quality results from a recently developed method using a neural network that has been pretrained for object recognition to iteratively capture the style using a global statistic. It does work for every content and style combination (multi-style), but requires minutes of computation. One way to improve the speed is to move the computation to training time by using feedforward neural networks. They deliver styled images in realtime with similar quality. Unfortunately, one network has to be trained for every single style (single-style).    
Our goal is to create a multi-style feedforward network optimizing the same novel global statistic as the iterative approach. Existing solutions either do not allow the input of arbitrary styles or are slower. We explore the limitations of a state of the art single-style network with a naive concatenation of style and content image at its input, so it accepts arbitrary content and style images. When trained with ten styles it mainly captures the colors and some artifacts that are copied from the style image.    
We contribute two different methods to overcome those limitations for at least ten styles. The first is using the single-style networks to supervise the learning of the multi-style network, which requires trained single-style networks but the quality is on par with them. The other method increases the initial influence of the content image and discourages results close to the style image. It trains as fast as the single-style variants. Both resulting networks allow realtime or mobile generation of a pastiche for at least ten styles. -->





<!-- Creating a new image with some content of choice that imitates the style of one of the great artists is something many people love. Creating such a pastiche manually is a challenging and time consuming task even for a talented artist. Therefore, a lot of work has been invested to automatically synthesize a pastiche. Many of the earlier methods lack a high level concept of what is important in an image and rely on pixel wise comparison.      
A stunning improvement in quality results from a recently developed method using a neural network that has been pretrained for object recognition to capture the style using a global statistic. The method iteratively optimizes a random image using said statistic until the desired pastiche emerges. It does work for every content and style combination (multi-style), but requires minutes of computation. One way to improve the speed is to move the computation to training time by using feedforward neural networks. They deliver styled images in realtime with similar quality. Unfortunately, one network has to be trained for every single style (single-style). Our goal is to create a multi-style feedforward network optimizing the same novel global statistic as the iterative approach. Existing solutions either do not allow the input of arbitrary styles, are slower or do not optimize the same statistic as the iterative method.
We explore the limitations of a state of the art single-style network with a naive concatenation of style and content image at its input, so it accepts arbitrary content and style images. When trained with ten styles it mainly captures the colors and some artifacts that are copied from the style image.    
We contribute two different methods to overcome those limitations for at least ten styles. The first is using the single-style networks to supervise the learning of the multi-style network, which requires trained single-style networks but the quality is on par with them. The other method increases the initial influence of the content image and trains as fast as the single-style variants, but the style-image might appear partially in the background of the pastiche.
Both resulting networks allow realtime or mobile generation of a pastiche for at least ten styles. -->





<!-- \vspace{4cm}
\qqqq{Question:}
*So, there is one paper that was released 4 weeks ago. It does exactly what we want to do. They even use a method very close to one of our failed attempts. They use the forementioned statistic as an input to the network, so they need it at generation time.  My working results are independent of that network and are therefore about 3 times faster. Their method trains directly for 10 styles and reaches single-style quality.
I am really unsure if this is covered by the fat marked sentence. Do I need to mention it at all?*

Current variant:
- Existing solutions either do not allow the input of arbitrary styles or have to modify the style input slowing down pastiche generation.

Other variants:
- Existing solutions do not allow direct style input into the generator network.
- Existing solutions do not allow direct style input.
- Existing solutions either do not allow direct style input or are slower. -->


<!-- Some solutions to overcome that limitation have been proposed, but none of them is able to create a pastiche directly from style and content (multi-style) like the original iterative approach or they are not using the same global statistic and consequently look different.  

result contribution benefit

\vspace{3cm}
\qqqq{TODOs:}

- \qqqq{**Motivation, problem statement, approach, evaluation, conclusion, explorative** -->

<!-- - \qqqq{**what about the parallel work**} -->
<!-- - \qqqq{**A recent app earned an "App of the Year (2016)" award for this.**} -->









\pagenumbering{roman}
\setcounter{page}{1}
