
\renewcommand{\mpwidth}{2.80cm}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2starry_night.png}%
  \inclimg{#1#2famosas-crop.png}%
  \inclimg{#1#2feathers.png}%
  \inclimg{#1#2la_muse.png}%  
  \inclimg{#1#2candy.png}%
  }

\renewcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2wave.png}%  
  \inclimg{#1#2composition_vii.png}%    
  \inclimg{#1#2mosaic.png}%
  \inclimg{#1#2the_scream.png}%
  \inclimg{#1#2udnie.png}%
}

\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.275cm}%
      \\ %
}


\begin{figure}[!htbp]
  \centering

  \newcommand{\many}[2]{source/results/many/MyStarry#2#140k/}

  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\combined}{source/results/combined/combine_punish_lc12to1_TenStyles40k/}  
  \newcommand{\reference}{source/results/supervision/InstanceMultiStyle40k/}
  \newcommand{\referenceEighty}{source/results/supervision/InstanceMultiStyle80k/}
  \newcommand{\referenceOneTwenty}{source/results/supervision/InstanceMultiStyle120k/}
  \newcommand{\linlc}{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/}
  \newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}  
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\iterative}{source/results/iterative/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.8cm}%
    \captionsetup{justification=centering}%%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}}}

  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\istyles}{}
    \captionv{Style image used as input, one or all used during training}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/lena_color.png}
    \inclTenStyleOne{\combined}{lena_colorX}
    \captionv{\textit{Ten-Styles Dataset} including all styles during test and training}  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\many{painters}{32}}{lena_colorX}
    \captionv{31 styles from the Painters Dataset and Starry Night}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\many{painters}{100}}{lena_colorX}
    \captionv{100 styles from the Painters Dataset and Starry Night}  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\many{painters}{200}}{lena_colorX}
    \captionv{200 styles from the Painters Dataset and Starry Night}  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\many{painters}{500}}{lena_colorX}
    \captionv{500 styles from the Painters Dataset and Starry Night}  
  \end{minipage}
  \hfill

  \caption[Visual comparison of the combined approach trained with an increasing number of style images]{
    All images produced by our \dd*combined* approach. The networks were trained on different datasets. All datasets include the \dd*Starry Night* style image and it is consequently used during training. We can see that for the ten and 32 styles datasets the pastiches from the \dd*Starry Night* style image have about equal quality. The quality declined using 101 style images and regressed to color transfer and \dd*style blending* for 201 and more images.\\
    The networks that were learned with more than ten styles did not train on the \dd*Famosa* style, but they are still able to generate pastiches close to the results from our \dd*combined-ten-styles network*. All other input styles produce results that we do not consider close, but we see more than simple style blending.\\
    Input styles from top to bottom: \dd*Starry Night*, \dd*Famosa*, \dd*Feathers*, \dd*A Muse* and \dd*Candy*.
  }

  \label{fig:visual-combined-generalizability}
\end{figure}

<!-- \cleardoublepage -->
