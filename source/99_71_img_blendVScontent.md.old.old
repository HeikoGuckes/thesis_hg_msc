\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=2.9cm, height=2.9cm, #1]%
      {#2}%
      \\ \vspace{-0.175cm}%
      \\ %
}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%
  \inclimg{#1#2composition_vii.png}%
  \inclimg{#1#2famosas-crop.png}%
  \inclimg{#1#2wave.png}%
  \inclimg{#1#2feathers.png}%
  %\inclimg{#1#2la_muse.png}%
  %\inclimg{#1#2mosaic.png}%
  %\inclimg{#1#2starry_night.png}%
  %\inclimg{#1#2the_scream.png}%
  %\inclimg{#1#2udnie.png}%
}

\renewcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  %\inclimg{#1#2candy.png}%
  %\inclimg{#1#2composition_vii.png}%
  %\inclimg{#1#2famosas-crop.png}%
  %\inclimg{#1#2wave.png}%
  %\inclimg{#1#2feathers.png}%
  \inclimg{#1#2la_muse.png}%
  \inclimg{#1#2mosaic.png}%
  \inclimg{#1#2starry_night.png}%
  \inclimg{#1#2the_scream.png}%
  \inclimg{#1#2udnie.png}%
}


\begin{figure}[htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\multiFourty}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}


  \begin{minipage}{3cm}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\istyles}{}
    \caption*{Style image}
	\end{minipage}
  \quad
  \begin{minipage}{3cm}
    \inclimg{source/images/content/chicago.png}
    \inclTenStyleTwo{\single}{chicagoX}
    \caption*{Single-style 40k}
	\end{minipage}
  \quad
  \begin{minipage}{3cm}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\multiFourty}{chicagoX}
    \caption*{$\lambda$-b Multi-style 40k}
	\end{minipage}
  \quad
  \begin{minipage}{3cm}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\naive}{chicagoX}
    \caption*{Concatenation 40k}
	\end{minipage}
  \hfill

  \caption[Part1: Output image comparison of a penalizing-multi-style network with single-style]{Part1: Comparison of the results of ten single-style networks with a penalizing-multi-style network and the concatenation network. All of them trained with 40k parameter updates.
  We can see that the blend loss enables the network to learn style features and the overall result is closer to the single-style baseline.}

  \label{fig:images-punish-blend-part1}
\end{figure}


\begin{figure}[htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\multiFourty}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}

  \begin{minipage}{3cm}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\istyles}{}
    \caption*{Style image}
	\end{minipage}
  \quad
  \begin{minipage}{3cm}
    \inclimg{source/images/content/chicago.png}
    \inclTenStyleOne{\single}{chicagoX}
    \caption*{Single-style 40k}
	\end{minipage}
  \quad
  \begin{minipage}{3cm}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\multiFourty}{chicagoX}
    \caption*{$\lambda$-b Multi-style 40k}
	\end{minipage}
  \quad
  \begin{minipage}{3cm}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\naive}{chicagoX}
    \caption*{Concatenation 40k}
	\end{minipage}
  \hfill

  \caption[Part2: Output image comparison of a penalizing-multi-style network with single-style]{Part2: Comparison of the results of ten single-style networks with a penalizing-multi-style network and the concatenation network. All of them trained with 40k parameter updates.
  We can see that the blend loss enables the network to learn style features and the overall result is closer to the single-style baseline.}

  \label{fig:images-punish-blend-part2}
\end{figure}
