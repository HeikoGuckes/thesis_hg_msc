\renewcommand{\el}{\mathcal{L}}
\renewcommand{\els}{\el_{s}}
\renewcommand{\elc}{\el_{c}}
\renewcommand{\ls}{\lambda_{s}}
\renewcommand{\lc}{\lambda_{c}}
\renewcommand{\contentlayers}{C}
\renewcommand{\stylelayers}{S}


### Fast Style Transfer Using Feedforward Neural Networks

\begin{figure}[b]
  \centering
  \includegraphics[width=0.7\textwidth]{source/architecture/SingleStyleNet.pdf}
  \caption[System overview for training a single-style network]{
    System overview for training a single-style network
  }
  \label{fig:single-style-architectural-overview}
\end{figure}


The iterative method produces very good results and works for arbitrary pairs of content and style images (multi-style) but is comparatively slow as it requires minutes to produce one pastiche. Therefore, different solutions were proposed to improve the speed. The patch-based solution from @PatchBasedUniMainz2016 keeps the flexibility for arbitrary styles. It relies on a feature-patch statistic which has some advantages for photorealistic style transfer and consequently their results look different compared to the loss function from @gatys.
@Dosovitskiy2015 are using a per-pixel reconstruction loss to train a feed-forward neural network for function approximation. @Johnson and @Ulyanov use the same approximation idea but instead of a per-pixel loss they combine it with the loss from @gatys, which is the technical foundation for this thesis. The main drawback of the feed-forward method is the limitation of one style per network, but the quality is close to the iterative method and about three magnitudes faster. This enables realtime video and mobile applications.

We prefer the optical results from @Johnson and therefore focus on their architecture. They train a deep residual convolutional neural network to do the style transfer. They are using convolutions with stride two to downsample the image for a core of 5 residual blocks before it is upsampled using deconvolution and outputs an image again. Most convolutions are followed by batch normalization and ReLU nonlinearities. The exact architecture can be found in their paper and our modified version in [@Sec:single-style-architecture]. The system overview can be found in [@Fig:single-style-architectural-overview]. It consists of the loss function $\el(s,c,p)$ with the underlying loss network $\phi$ and an image transformation network $f_W$ with just the content $c$ as its input. It transforms the input content image $c$ to an output image $\hat y$ using the mapping $\hat y = f_W(c)$. After the training the style $s$ is incorporated within the transformation network weights $W$. We notate weights for a single style network trained with style $s$ as $W_s$. The image transformation network is trained using the Adam optimizer [@adam] to minimize the loss function with tv loss [@Eq:loss-function-tv], which results in optimized weights:

$$
W_s^{*} = \arg\min_W \mathbb{E}_{ s, c }
\biggl[  
   \el(s, c, f_w(c))
\biggr]
$$ {#eq:optimize-forward-loss}

The quality of the forward method can be improved by replacing all batch normalization layers with instance normalization [@instance]. The methods are very closely related and differ only in the fact that instance normalization only normalizes per image in a batch, whereas batch normalization uses the complete batch.

<!--  MAYBE: add formulas, they are short-->

To overcome the limitation of training one network per style @google2016 used the instance-normalized version and trained the network for 32 styles by sharing about 98% of the parameters of the network between styles but switching to an individual instance normalization layer per style. We consider this a hybrid approach between single and multi-style. It is very interesting that only the variance of some normalization/rescaling parameters in the instance-normalization layer is enough to produce such a variety of style. A very nice property of their method is that the training time is not increased compared to a single style. It also allows for very nice blending between different styles with simple parameters, but it can not produce styled images for untrained styles.

Additional improvements to the architecture from @Johnson are:

(1) The introduction of mirror-padding to avoid border artifacts instead of SAME-padded convolutions [@google2016]
(2) The replacement of deconvolution with nearest-neighbor upscaling combined with a convolution to avoid checkerboard artifacts [@checkerboard].

<!-- We call the network architecture from @Johnson including all three improvements the *single-style network*. -->

The contribution of this work is to show that the single-style network is in fact capable of simultaneously learning multiple-styles. To achieve this we explore the properties of the single-style network when confronted with multiple styles which leads to two different successful approaches to overcome the single-style limitation. The first approach combines two ideas: (1) By controlling the influence of the content image we prevent the network from focussing too much on the style input and (2) by discouraging pixelwise similarity to the style image at the output of the loss network we produce pastiches that are similar and not equal to the style image. The second approach is supervising the training of the multi-style network with the output from  single-style networks. We show that the networks are capable of learning at least ten different styles and produce similar quality as the single-style networks.

Very recent parallel work has the same goal and proposes a solution that introduces a completely different architecture [@zhang2017multistyle]. They use the Gram of the output of the loss network $G(\phi_j)$ as an additional input for the image transformation network at multiple layers. This is a variant that is conceptually comparable to our dimensionality reduction approach, but we unsuccessfully used PCA and concatenation, where they succeeded by introducing a new type of layer using matrix multiplication to incorporate the transformed style information. Our successful versions have the advantage that they do not need the input from the loss network and are therefore as fast as the single-style networks at test time.



<!-- ###   Fast Patch-based Style Transfer of Arbitrary Style
  [@FastPatchBased]
  - a true multi-style but completely different approach, should definitely get a mention,
  - maybe just mention this in a more generic overview like the introduction to this part -->
