#!/usr/bin/env bash

declare -a arr=("candy" "composition_vii" "famosas" "feathers" "la_muse" "mosaic" "starry" "scream" "udnie" "wave")

for i in "${arr[@]}"
do
  {
  echo "processing:$i"

  python plot.py \
              concatenation*.txt \
              penalize*.txt \
              --style_filter "$i"  -pl 01100 --ylim 0.5e5 6e5 --max_iter 40000

#  python plot.py \
#              single*"$i"*.txt \
#              concatenation*.txt \
#              penalize*.txt \
#              --style_filter "$i"  -pl 01100 --ylim 0.5e5 3e5 --max_iter 40000

  } &
done




