#!/usr/bin/env bash



python plot.py \
          concatenation*.txt \
          single*.txt \
          supervision*.txt \
          combine*.txt \
          --style_filter average  -pl 10000 --ylim 2.5e5 10e5 --max_iter 120000 --show&


python plot.py \
              concatenation*.txt \
              single*.txt \
              supervision*.txt \
              combine*.txt \
              --style_filter average  -pl 01000 --ylim 0.5e5 4e5 --max_iter 120000 --show&

python plot.py \
              concatenation*.txt \
              single*.txt \
              supervision*.txt \
              combine*.txt \
              --style_filter average  -pl 00100 --ylim 2e5 5e5 --max_iter 120000 --show&


#  python plot.py \
#              single*"$i"*.txt \
#              concatenation*.txt \
#              penalize*.txt \
#              --style_filter "$i"  -pl 01100 --ylim 0.5e5 3e5 --max_iter 40000





