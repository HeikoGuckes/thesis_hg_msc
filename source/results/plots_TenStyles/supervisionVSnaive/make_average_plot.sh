#!/usr/bin/env bash



  python ../plot.py \
              ../concatenation*.txt \
              ../supervision*.txt \
              --style_filter average  -pl 01000 --ylim 0.5e5 5e5 --max_iter 120000 &

  python ../plot.py \
              ../concatenation*.txt \
              ../supervision*.txt \
              --style_filter average  -pl 01110 --ylim 0.5e5 5e5 --max_iter 120000 &

#  python plot.py \
#              single*"$i"*.txt \
#              concatenation*.txt \
#              penalize*.txt \
#              --style_filter "$i"  -pl 01100 --ylim 0.5e5 3e5 --max_iter 40000





<