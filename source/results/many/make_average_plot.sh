#!/usr/bin/env bash



  python plot.py \
              *params.txt \
              --style_filter starry  -pl 01000 --ylim 0.5e5 2.5e6 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter starry  -pl 00100 --ylim 0.5e5 1.5e6 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter starry  -pl 00010 --ylim 1.5e5 3e5 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter starry  -pl 10000 --ylim 0.5e5 2.5e6 --max_iter 40000 &



  python plot.py \
              *params.txt \
              --style_filter mosaic  -pl 01000 --ylim 0.5e5 8e6 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter mosaic  -pl 00100 --ylim 0.5e5 1.5e6 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter mosaic  -pl 00010 --ylim 1.5e5 3e5 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter mosaic  -pl 10000 --ylim 0.5e5 8e6 --max_iter 40000 &



  python plot.py \
              *params.txt \
              --style_filter average  -pl 01000 --ylim 0.5e5 3e6 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter average  -pl 00100 --ylim 0.5e5 1.5e6 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter average  -pl 00010 --ylim 1e5 3e5 --max_iter 40000 &

  python plot.py \
              *params.txt \
              --style_filter average  -pl 10000 --ylim 0.5e5 3e6 --max_iter 40000 &







