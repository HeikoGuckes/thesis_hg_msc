import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
import pylab

blue = '#1f78b4'
lblue = '#a6cee3'
green = '#33a02c'
lgreen = '#b2df8a'

def smooth(x, window_len=11, window='flat'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."

    if window_len < 3:
        return x

    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"

    s = np.r_[x[window_len - 1:0:-1], x, x[-1:-window_len:-1]]
    # print(len(s))
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('numpy.' + window + '(window_len)')

    y = np.convolve(w / w.sum(), s, mode='same')
    return y[window_len - 1:-(window_len - 1)]


def create_plot_from_args(args):
    y_label = 'Loss'
    if args.plot_losses == '01000':
        y_label = 'Style loss'
    elif args.plot_losses == '00100':
        y_label = 'Content loss'
    elif args.plot_losses == '00010':
        # y_label = 'Blend loss'
        y_label = 'Reference loss'
    elif args.plot_losses == '00001':
        y_label = 'Total variation loss'
    add_prefix = y_label == 'Loss'

    create_plots(logfiles=args.logfile,
                 out_file_name=args.output,
                 display_plot=args.show,
                 dontsave=args.show,
                 main_title='',
                 y_label=y_label,
                 add_prefix=add_prefix,
                 plot_lc=args.plot_losses[2] == '1',
                 plot_lref=args.plot_losses[3] == '1',
                 plot_ls=args.plot_losses[1] == '1',
                 plot_ltv=args.plot_losses[4] == '1',
                 plot_total=args.plot_losses[0] == '1',
                 plot_train=args.plot_train,
                 plot_validation_mean=args.hide_mean is False,
                 plot_validation_std=args.plot_std,
                 max_iter=args.max_iter,
                 style_filter=args.style_filter,
                 y_lim=args.ylim)


def create_plots(logfiles, out_file_name, display_plot, dontsave, main_title,
                 y_label, add_prefix,
                 plot_lc, plot_lref, plot_ls,
                 plot_ltv, plot_total, plot_train, plot_validation_mean, plot_validation_std,
                 max_iter, style_filter, y_lim):
    pattern = re.compile(".* it:(.*)/.* (.*)=sum.* (.*)=ls (.*)=lc  (.*)=lref (.*)=ltv  ?(.*)")

    print("Fetch data")
    data_dict = {}
    for logfile in logfiles:
        file_data = pattern.findall(logfile.read())

        print("Reading" + logfile.name)
        for line in file_data:
            # the last entry either starts with an @ which is the loss on the training data
            # or it starts with vmean:<stylename> which is the mean of the loss on the validation set for <stylename>
            # or it starts with  vstd:<stylename> which is the std of the loss on the validation set for <stylename>
            data_dict.setdefault(line[6], []).append(line[0:6])
            iteration = int(line[0])
            if 0 < max_iter < iteration:
                break

    add_suffix, y_label = add_the_type_of_the_data_to_the_legend_or_just_add_it_to_the_y_label(plot_train,
                                                                                               plot_validation_mean,
                                                                                               plot_validation_std,
                                                                                               y_label)

    print("Plotting data")
    fig = pylab.figure(dpi=150, figsize=(6.8, 4.8))
    plot = fig.add_subplot(111)
    plot.set_xlabel('Parameter updates')
    plot.set_ylabel(y_label)
    plot.set_title(main_title)

    for legend, file_data in sorted(data_dict.items()):
        print("working on:" + legend)

        # Determine smoothing, smooth training data a lot more
        if legend.startswith('@'):
            def smoooth(d):
                return smooth(d, 101)
        else:
            def smoooth(d):
                return smooth(d, 5)

        # Do not plot unwanted data
        if legend.startswith('@'):
            if not plot_train:
                continue
        elif legend.startswith('vmean'):
            if not plot_validation_mean:
                continue
        elif legend.startswith(' vstd'):
            # if not plot_validation_std:
            continue

        if style_filter not in legend:
            continue

        # search for available std:
        if legend.startswith('vmean'):
            error_legend = legend.replace('vmean', ' vstd')
            error_data = data_dict[error_legend]
        else:
            error_data = None


        # Renaming for the legend
        suffix = ''  # added to the name of the style in the legend
        if legend.startswith('@'):
            legend = ''
            suffix = 'smoothed training'
        elif legend.startswith('vmean'):
            legend = legend.replace('vmean:X', '')
            legend = legend.replace('vmean:', '')
            suffix = ' $\mu$-val'
        elif legend.startswith(' vstd'):
            legend = legend.replace(' vstd:X', '')
            legend = legend.replace(' vstd:', '')
            suffix = ' $\sigma$-val'
        if add_suffix:
            legend += suffix

        # give our data names and put the into columns
        iteration = [int(row[0]) for row in file_data]
        ltotal = [float(row[1]) for row in file_data]
        etotal = [float(row[1]) for row in error_data]
        lstyle = [float(row[2]) for row in file_data]
        estyle = [float(row[2]) for row in error_data]
        lcontent = [float(row[3]) for row in file_data]
        econtent = [float(row[3]) for row in error_data]
        lref = [float(row[4]) for row in file_data]
        eref = [float(row[4]) for row in error_data]
        ltv = [float(row[5]) for row in file_data]
        etv = [float(row[5]) for row in error_data]

        def p(x, y, e, label):
            result = plot.errorbar(x, y, yerr=None, errorevery=1, label=label,
                                   # http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.errorbar
                                   fmt='',  # plot graph and errorbars

                                   # graph line parameters
                                   linestyle='solid',  ##0F0F0F0F
                                   # ['solid' | 'dashed', 'dashdot', 'dotted' | (offset, on-off-dash-seq) | '-' | '--' | '-.' | ':' | 'None' | ' ' | '']
                                   linewidth=1,
                                   color=blue,

                                   # errorbar parameters
                                   elinewidth=0.00000025,
                                   ecolor='red',
                                   capsize=1, capthick=1,
                                   )

            plot.fill_between(x, y - e, y + e,  # 1:-1 hack to avoid fat white borders
                              alpha=1.0, edgecolor=lblue, facecolor=lblue)

            return result



        # plot it
        if plot_total:
            plot_plot = p(iter, smoooth(np.array(ltotal)), smooth(np.array(etotal)), label='$\mathcal{L}$ ')
        if plot_ls and sum(lstyle) > 0:
            prefix = '$\mathcal{L}_s$ ' if add_prefix else ''
            p(iteration, smoooth(np.array(lstyle)), smoooth(np.array(estyle)), label='40k multi-style single-content chicago')
            # plot_baseline(plot, 109982, 72313)
            pylab.ylim([0, 4e6])
        if plot_lref and sum(lref) > 0:
            prefix = '$\mathcal{L}_c$ ' if add_prefix else ''
            p(iteration, smoooth(np.array(lref)), smoooth(np.array(eref)), label=prefix + legend)
        if plot_lc and sum(lcontent) > 0:
            prefix = '$\mathcal{L}_c$ ' if add_prefix else ''
            p(iteration, smoooth(np.array(lcontent)), smoooth(np.array(econtent)), label='40k multi-style single-content chicago')
            # plot_baseline(plot, 224879, 128901)
            pylab.ylim([0, 0.7e6])
        if plot_ltv and sum(ltv) > 0:
            prefix = '$\mathcal{L}_tv$  ' if add_prefix else ''
            p(iteration, smoooth(np.array(ltv)), smoooth(np.array(etv)), label=prefix + legend)


        plot.legend()

    plt.legend(fontsize="medium", labelspacing=0.3)  # using a named size
    plt.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
    # pylab.ylim([0, 0.7e6])
    # pylab.ylim([0.5e2, 2.3e2])
    # pylab.ylim(y_lim)
    pylab.xlim([-300, 40000+300])

    if not dontsave:
        if out_file_name is None:
            if len(logfiles) == 1:
                out_file_name = logfiles[0].name + '_' + y_label + '.pdf'
            else:
                out_file_name = 'plot_' + y_label + '.pdf'
        out_file_name = out_file_name.replace('.txt', '_').replace(' ', '_')
        print("saved:" + out_file_name)
        fig.savefig(out_file_name, bbox_inches='tight')  # pad_inches

    if display_plot:
        plt.show()


def plot_baseline(plot, mean, std):
    p = plot.errorbar(np.array(range(0, 40000)),
                      np.ones((40000,)) * mean,
                      color=green, linewidth=1,
                      yerr=np.ones((40000,)) * std,
                      errorevery=19999,
                      ecolor=green,
                      capsize=5, capthick=1,
                      label='ten single-style multi-content 40k updates baseline')


def add_the_type_of_the_data_to_the_legend_or_just_add_it_to_the_y_label(plot_train, plot_validation_mean,
                                                                         plot_validation_std, y_label):
    add_suffix = True
    plot_tms = (plot_train, plot_validation_mean, plot_validation_std)
    if plot_tms == (True, False, False):
        add_suffix = False
        y_label += ' while training'
    elif plot_tms == (False, True, False):
        add_suffix = False
        y_label += ' mean on the validation set'
    elif plot_tms == (False, False, True):
        add_suffix = False
        y_label += ' standard deviation (std) on the validation set'
    return add_suffix, y_label


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate real time style transfer models for fast_generate.py')

    parser.add_argument('--show', action='store_true',
                        help='if you want to display the result')

    parser.add_argument('--max_iter', '-x', default=0, type=int)

    parser.add_argument('logfile', type=argparse.FileType('r'), nargs='+')

    parser.add_argument('--output', '-o', type=str,
                        help='for single file inputs defaults to filename+pdf otherwise will save at pwd')

    parser.add_argument('--plot_losses', '-pl', type=str, default='01111',
                        help='01111 for ltotal=false, ls, lc, lref, ltv')

    parser.add_argument('--plot_train', action='store_true', default=False)
    parser.add_argument('--hide_mean', action='store_true', default=False)
    parser.add_argument('--plot_std', action='store_true', default=False)

    parser.add_argument('--style_filter', '-filter', type=str, default='',
                        help='type candy if you only want to plot the candy style ')

    parser.add_argument("--ylim", nargs=2, metavar=('y_lim_low', 'y_lim_high'),
                        help="limits the y axis", type=float,
                        default=[0, 0.7e6])

    pargs = parser.parse_args()

    create_plot_from_args(pargs)
