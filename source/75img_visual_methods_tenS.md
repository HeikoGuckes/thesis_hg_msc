<!-- ### Visual -->
\subparagraph*{Visual}\nop{*}

For the final visual comparison we choose the *Lena* content image as it contains fine details like eyes and hair, but also has some bigger elements like the hat. [@fig:visual-all-methods-ten-styles-part1] and [@fig:visual-all-methods-ten-styles-part2] show our results for all Styles of the *Ten-Styles Dataset*.

We perceive more intensive and more colors for the iterative approach, e.g.\ for the *Candy* and *Mosaic* Style. The non iterative (feed-forward) approaches preserve fine details from the content image better, e.g.\  *Lena's* nose and eyes for *A Muse*, *Mosaic* and *Candy* Style. The face as a whole is more consistent. This is intuitive as the feed-forward networks are trained on the *MS COCO Dataset*, which consists of a lot of natural images containing persons and common objects. The 120k supervision approach is slightly less sharp than the single-style approach. We do not consider it so drastic that a pastiche is clearly inferior. Overall we consider all pastiches to be of approximately equal quality.





\renewcommand{\mpwidth}{3.30cm}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%
  \inclimg{#1#2composition_vii.png}%
  \inclimg{#1#2famosas-crop.png}%
  \inclimg{#1#2wave.png}%
  \inclimg{#1#2feathers.png}%
  %\inclimg{#1#2la_muse.png}%
  %\inclimg{#1#2mosaic.png}%
  %\inclimg{#1#2starry_night.png}%
  %\inclimg{#1#2the_scream.png}%
  %\inclimg{#1#2udnie.png}%
}


\renewcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  %\inclimg{#1#2candy.png}%
  %\inclimg{#1#2composition_vii.png}%
  %\inclimg{#1#2famosas-crop.png}%
  %\inclimg{#1#2wave.png}%
  %\inclimg{#1#2feathers.png}%
  \inclimg{#1#2la_muse.png}%
  \inclimg{#1#2mosaic.png}%
  \inclimg{#1#2starry_night.png}%
  \inclimg{#1#2the_scream.png}%
  \inclimg{#1#2udnie.png}%
}

\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.275cm}%
      \\ %
}



\begin{figure}[!htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\combined}{source/results/combined/combine_punish_lc12to1_TenStyles40k/}  
  \newcommand{\reference}{source/results/supervision/InstanceMultiStyle40k/}
  \newcommand{\referenceEighty}{source/results/supervision/InstanceMultiStyle80k/}
  \newcommand{\referenceOneTwenty}{source/results/supervision/InstanceMultiStyle120k/}
  \newcommand{\linlc}{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/}
  \newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}  
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\iterative}{source/results/iterative/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.8cm}%
    \captionsetup{justification=centering}%%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}}}

  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\istyles}{}
    \captionv{Style image   \\  \ }  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/lena_color.png}
    \inclTenStyleTwo{\iterative}{lena_colorX}
    \captionv{Iterative   \\ (Gatys et al. (2016))}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\single}{lena_colorX}
    \captionv{Single-style (Modified Johnson et al., 2016)}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\referenceOneTwenty}{lena_colorX}
    \captionv{Supervision  \\ (Ours) \ }  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\combined}{lena_colorX}
    \captionv{Combined $\elc$-control and $\el_{b}$-penalize (Ours)}  
	\end{minipage}
  \hfill

  \caption[Comparison of the novel approaches with the iterative and single-style baseline (Part1)]{   
   (Part 1) Final visual comparison of our viable novel approaches with the iterative and single-style baseline. We consider all pastiches to be of approximately equal quality.
   \\Styles from top to bottom: \dd*A Muse*, \dd*Mosaic*, \dd*Starry Night*, \dd*The Scream*, \dd*Udnie*.
  }

  \label{fig:visual-all-methods-ten-styles-part1}
\end{figure}


\begin{figure}[!htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\combined}{source/results/combined/combine_punish_lc12to1_TenStyles40k/}  
  \newcommand{\reference}{source/results/supervision/InstanceMultiStyle40k/}
  \newcommand{\referenceEighty}{source/results/supervision/InstanceMultiStyle80k/}
  \newcommand{\referenceOneTwenty}{source/results/supervision/InstanceMultiStyle120k/}
  \newcommand{\linlc}{source/results/adaptive/adaptive_linear_TenStyles_1at30k40k/}
  \newcommand{\punish}{source/results/punish/PunishBlendVGGTenStyleslb200_40k/}  
  \newcommand{\naive}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\iterative}{source/results/iterative/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.8cm}%
    \captionsetup{justification=centering}%%
    \caption*{%
      \begin{scriptsize}
      #1%
      \end{scriptsize}}}

  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\istyles}{}
    \captionv{Style image   \\  \ }  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/lena_color.png}
    \inclTenStyleOne{\iterative}{lena_colorX}
    \captionv{Iterative   \\ (Gatys et al., 2016)}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\single}{lena_colorX}
    \captionv{Single-style (Modified Johnson et al., 2016)}  
	\end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\referenceOneTwenty}{lena_colorX}
    \captionv{Supervision  \\ (Ours) \ }  
  \end{minipage}
  \hfill
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\combined}{lena_colorX}
    \captionv{Combined $\elc$-control and $\el_{b}$-penalize (Ours)}  
	\end{minipage}
  \hfill

  \caption[Comparison of the novel approaches with the iterative and single-style baseline (Part2)]{   
   (Part 2) Final visual comparison of our viable novel approaches with the iterative and single-style baseline. We consider all pastiches to be of approximately equal quality.\\
   Styles from top to bottom: \dd*Candy*, \dd*Composition*, \dd*Famosa*, \dd*Wave*, and \dd*Feathers*.
  }

  \label{fig:visual-all-methods-ten-styles-part2}
\end{figure}


<!-- \cleardoublepage -->
