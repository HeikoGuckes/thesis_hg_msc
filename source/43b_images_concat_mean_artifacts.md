\renewcommand{\mpwidth}{2.80cm}
\renewcommand{\mphspace}{\hspace{-0cm}}

\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.875cm}%
      \\ %                
}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2composition_vii.png}%  
}

\renewcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2candy.png}%
}

\begin{figure}[htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\naiveFourty}{source/results/naive/naiveCocoXTenStyles40k/}
  \newcommand{\multiFourty}{source/results/naive/naiveCocoXTenStyles40k/}

  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\istyles}{}
    \caption*{Candy Style}  
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\naiveFourty}{chicagoX}
    \caption*{Candy@Chicago}
	\end{minipage}  
  \mphspace
  \hspace{0.015cm}
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\multiFourty}{lena_colorX}
    \caption*{Candy@Lena}
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\naiveFourty}{meanX}
    \caption*{Mean Pastiche}  
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\multiFourty}{artifactsX}
    \caption*{Invariance}  
	\end{minipage}
  \hfill

  \caption[Visualization of the style copy problem]{      
      Results from the \dd*ten-styles-concatenation network*. This visualizes the \dd*style copy artifacts* problem introduced by the naive concatenation approach. When learned with multiple styles, some elements are copied into every pastiche regardless of the content image. It is not hard to spot the inspiration for the artifacts from the \dd*Candy* style image. The problem becomes especially apparent when looking at the mean image over multiple pastiches generated with the same style image (Mean Pastiche). We can also set the non-similar colors in each color channel to 0 and the similar colors ($\pm 3$) to 255 to get an image representing the invariance over the three content images. This network copies about 12.6\% similar pixels into every pastiche for the \dd*Candy* style and an average of 7.3\% for all ten styles. See Fig. \ref{fig:naive-blending-problem} for all ten styles.
    }

  \label{fig:images-concat-mean-artifacts}
\end{figure}
