\newpage

## Comparison of the Novel Approaches with the Single-Style and Iterative Baseline

In the first part of the evaluation of the novel approaches we assessed the viability for every approach for multi-style for the *Ten-Styles Dataset*. We evaluated the combined approach to be viable when trained with 40k parameter updates and the supervision approach with 120k parameter updates. We dismissed the dimensionality reduction approach as it offered no benefit.

We compare the two resulting viable approaches with the single-style baseline and the iterative baseline. We decided to include the results for some of the subcomponents and the concatenation approach for additional reference where applicable.
