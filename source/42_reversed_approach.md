\newpage

## The Reversed Approach: Single-Content Instead of Single-Style {#sec:reversed-approach}
<!-- Multi-style Single-content Network with only Style Input -->

The single-style network with parameters $W_s$ has to infer and incorporate the style (or how to style) into its weights during training. We wanted to know if the network is able to incorporate a single content image instead and achieve multi-style in this simpler setting. If this works we expect our network to transfer arbitrary styles on the fixed content image.

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{source/architecture/SingleContentNet.pdf}
  \caption[System overview for training a the reversed approach single-content multi-style transfer network]{
    System overview for training the \dd*reversed approach* single-content multi-style transfer network.
  }
  \label{fig:single-content-architectural-overview}
\end{figure}

\subparagraph*{Method}\nop{*} In the single-style system the style and content image are used by the loss network anyway. We just change which image is going into the transformation network. The *single-content* system wants to train an actual multi-style network, but only for a single content image. We choose the same *architecture* as for training the single-style networks, but instead of the content image $c$ we use the style $s$ as input for the transformation network $f_w(s)$. The system overview is shown in [@Fig:single-content-architectural-overview]. In consequence training minimizes the modified loss function:

$$
W_c^{*} = \arg\min_W \mathbb{E}_{ s, c }
\biggl[  
   \el(s, c, f_w(s))
\biggr]
$$
where $W_c$ denotes a network with weights trained for content $c$.

We choose the *Chicago* image as our content image $c$. We have to train the network using a training data set with different styles $S$, so the *Painters Style Dataset* comes into play. The validation set is switched accordingly, so we calculate the mean over $128$ validation style images. We call the resulting network the *chicago-inferred-painters network*.   
Additionally we use our *Ten-Styles Dataset*. We do not expect the resulting network to generalize to other styles and therefore evaluate the mean over all ten styles for our content image. We call the resulting network the *chicago-inferred-ten-styles network*.\
All other parameters are the same as the single-style setup.


<!--  ---------------------------------------  -->
\subparagraph*{Results}\nop{*}

\begin{figure}
  \centering
  \includegraphics[height=0.23\textheight]{source/results/ConstantContentFigures/constant_content_chicago_b4_ls5_lc1_TenStyles_params__Content_loss_mean_on_the_validation_set.pdf}
  \hfill
  \includegraphics[height=0.23\textheight]{source/results/ConstantContentFigures/constant_content_chicago_b4_ls5_lc1_TenStyles_params__Style_loss_mean_on_the_validation_set.pdf}
  \caption[Loss comparison of the reversed approach with the single-style baseline]{
    The \dd*chicago-inferred-ten-styles network* (blue) from the reversed approach is actually able to reach lower style losses (right) than the \dd*single-style network* (green) while the content loss (left) for this specific content image is about the same. The standard deviation (light color) in this plot is over different styles.
  }
  \label{fig:single-content-graph}
\end{figure}


\begin{figure}
  \centering
  \includegraphics[height=0.23\textheight]{source/results/constant_content_chicago_b4_ls5_lc1_painters/constant_content_chicago_b4_ls5_lc1_painters_params__Content_loss_mean_on_the_validation_set.pdf}
  \hfill
  \includegraphics[height=0.23\textheight]{source/results/constant_content_chicago_b4_ls5_lc1_painters/constant_content_chicago_b4_ls5_lc1_painters_params__Style_loss_mean_on_the_validation_set.pdf}
  \caption[Losses of the reversed approach on the painters dataset]{
    Content loss (left) and style loss (right) during training of the \dd*chicago-inferred-painters network* using 80k images from the \dd*Painters Dataset* and the \dd*Chicago* content image.
    The network is not able to get close to the content image resulting in a flat content loss (left). The style loss is also not even close to the levels of the single-style networks (note the different scale). The standard deviation (light color) in this plot is over different styles.
  }
  \label{fig:single-content-kaggle-graph}
\end{figure}

The resulting images for the *chicago-inferred-ten-styles network* can be seen in [@Fig:single-content-images]. For \dd*Starry Night* and \dd*Famosa* in the second row the network produces a sharp but styled \dd*Chicago* skyline. For \dd*Mosaic* and \dd*Composition* in the first row the network does not get as close. Additionally, the network copies parts of the style image into the output image for the *A Muse* and *The Scream* style images. In [@Fig:single-content-graph] we can see that style and content loss reduce with more parameter updates. The single-content network is actually able to reach lower style losses while the content loss for this specific content image is (not significant) higher. This is consistent with our observation that some styles lead to networks that are not able to produce results close to the content.

For the *chicago-inferred-painters-network* the network is not able to get close to the content image as we can see from the loss function in [@Fig:single-content-kaggle-graph]. The content loss is simply flatlining. Instead, the network learns to output the given style image. This is also not alleviated by lowering $\ls$ from 5 to 0.5. This change only lowers the quality, so the outputted style image looks blocky.


\subparagraph*{Interpretation}\nop{*}

The result shows us that multi-style limited to single-content transfer is indeed feasible for our chosen network architecture. However, the method seems to be only applicable for specific styles. Some styles lead to a bad reconstruction of the content image. For the ten-styles experiment the network only has to learn ten different configurations (content combined with style). With this in mind we would expect better content and style loss than the single-style network baseline, but the content loss is worse.
In other cases the network is not able to get close to the content at all as the loss on the *Painters Style Dataset* in [@fig:single-content-kaggle-graph] showed us. Consequently, we need to improve the content loss if we want a sharper, better, content representation. This leads us to the concatenation approach where we feed the content as additional information together with the style image into the network.
