\renewcommand{\mpwidth}{2.80cm}
\renewcommand{\mphspace}{\hspace{-0cm}}

\renewcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.875cm}%
      \\ %                
}

\renewcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2composition_vii.png}%  
}

\renewcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  \inclimg{#1#2mosaic.png}%
}

\begin{figure}[htbp]
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\naiveFourty}{source/results/naive/naiveChicagoXTenStyles40k/}
  \newcommand{\multiFourty}{source/results/ConstantContant40k/}

  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \caption*{Content image}  
  \end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\naiveFourty}{chicagoX}
    \caption*{Concatenation}
	\end{minipage}  
  \mphspace
  \hspace{0.015cm}
  \begin{minipage}{\mpwidth}
    \inclTenStyleTwo{\multiFourty}{chicagoX}
    \caption*{Single-content}
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\naiveFourty}{chicagoX}
    \caption*{Concatenation}  
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclTenStyleOne{\multiFourty}{chicagoX}
    \caption*{Single-content}  
	\end{minipage}
  \hfill

  \caption[The concatenation approach improves the representation of the content image]{      
      The network is much closer to the content when the content image is put additionally into the network (\dd*concatenation-multi-style*) vs the network trying to infer the content from the loss function (\dd*chicago-inferred-content*). We only show the most problematic images from the \dd*Ten-Styles Dataset*.}

  \label{fig:images-concat-vs-reversed}
\end{figure}
