import numpy as np
import matplotlib.pyplot as plt
import pylab

x = np.arange(-2, 2, 0.1)

# Hyperbolic Tangent
y_tanh = np.tanh(x)

# Sigmoid/Softmax
sigmoid = lambda t: 1/(1+np.exp(-t))
sigmoid_vec = np.vectorize(sigmoid, otypes=[np.float])
y_sigmoid = sigmoid_vec(x)

# Rectified Linear Units
relu = lambda t: max(0, t)
relu_vec = np.vectorize(relu, otypes=[np.float])
y_relu = relu_vec(x)

# Leaky ReLU
leaky_relu = lambda t: max(0.1*t, t)
leaky_relu_vec = np.vectorize(leaky_relu, otypes=[np.float])
leaky_y_relu = leaky_relu_vec(x)

fig = pylab.figure(dpi=150, figsize=(4.8, 4.8))
plot = fig.add_subplot(111)

#plot.spines['left'].set_position('zero')
#plot.spines['bottom'].set_position('zero')
#plot.grid(True, which='both')
#plot.axhline(y=0, color='k')
#plot.axvline(x=0, color='k')

plot.set_xlabel('$x$')
plot.set_ylabel('$f(x)$')
plot.plot(x, y_relu, label='ReLU')
plot.plot(x, leaky_y_relu, '----',label='Leaky ReLU')
plot.plot(x, y_tanh, label='tanh')
plot.plot(x, y_sigmoid, label='Sigmoid')

plot.legend()



plt.show()