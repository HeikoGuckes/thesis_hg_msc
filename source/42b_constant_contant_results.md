\newcommand{\mpwidth}{2.80cm}
\newcommand{\mphspace}{\hspace{-0cm}}

\newcommand{\inclimg}[2][]{%
  \includegraphics[width=\mpwidth, height=\mpwidth, #1]%
      {#2}%
      \\ \vspace{-0.275cm}%
      \\ %                
}

\newcommand{\inclTenStyleOne}[2]{% #1 = path, #2 = content-name
  %\inclimg{#1#2candy.png}%  
  \inclimg{#1#2composition_vii.png}%  
  \inclimg{#1#2famosas-crop.png}%
  %\inclimg{#1#2wave.png}%
  %\inclimg{#1#2feathers.png}%
  %\inclimg{#1#2la_muse.png}%
  %\inclimg{#1#2mosaic.png}%
  %\inclimg{#1#2starry_night.png}%  
  %\inclimg{#1#2the_scream.png}%
  %\inclimg{#1#2udnie.png}%
}

\newcommand{\inclTenStyleTwo}[2]{% #1 = path, #2 = content-name
  %\inclimg{#1#2candy.png}%  
  %\inclimg{#1#2composition_vii.png}%  
  %\inclimg{#1#2famosas-crop.png}%
  %\inclimg{#1#2wave.png}%
  %\inclimg{#1#2feathers.png}%
  %\inclimg{#1#2la_muse.png}%
  \inclimg{#1#2mosaic.png}%
  \inclimg{#1#2starry_night.png}%  
  %\inclimg{#1#2the_scream.png}%
  %\inclimg{#1#2udnie.png}%
}

\begin{figure}
  \centering
  \newcommand{\istyles}{source/images/styles/}
  \newcommand{\single}{source/results/InstanceSingleStyle40k/}
  \newcommand{\multiTwenty}{source/results/ConstantContant20k/}
  \newcommand{\multiFourty}{source/results/ConstantContant40k/}
  \newcommand{\multiEighty}{source/results/ConstantContant80k/}

  \newcommand{\captionv}[1]{%
    \vspace{-0.8cm}%
    \caption*{%
      \begin{small}
      #1%
      \end{small}}}


  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleTwo{\istyles}{}
    \captionv{Style image}  
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \inclTenStyleTwo{\single}{chicagoX}
    \captionv{Single-style}  
	\end{minipage}  
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \inclTenStyleTwo{\multiFourty}{chicagoX}
    \captionv{Inferred}  
	\end{minipage}
  \mphspace
  \hspace{0.015cm}
  \begin{minipage}{\mpwidth}
    \inclimg{\istyles white_image.png}
    \inclTenStyleOne{\istyles}{}
    \captionv{Style image}  
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \inclTenStyleOne{\single}{chicagoX}
    \captionv{Single-style}  
	\end{minipage}
  \mphspace
  \begin{minipage}{\mpwidth}
    \inclimg{source/images/content/chicago.png}
    \inclTenStyleOne{\multiFourty}{chicagoX}
    \captionv{Inferred}  
	\end{minipage}
  \hfill



  \caption[Visual comparison of the single-style baseline and the single-content network]{    
    Comparison of the \dd*single-style baseline* and the \dd*reversed approach* \dd*chicago-inferred-ten-styles network*. For \dd*Starry Night* and \dd*Famosa* in the second row the network produces a sharp but styled \dd*Chicago* skyline. For \dd*Mosaic* and \dd*Composition* in the first row the network does not get as close.
  }
  \label{fig:single-content-images}
\end{figure}

<!-- The network is copying parts of the style image into the output image for some styles (A Muse, The Scream).} -->
