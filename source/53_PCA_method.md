<!-- \leavevmode\thispagestyle{empty}\newpage -->
## Dimensionality Reduction on the Style Image {#sec:pca-approach}
<!--  --------------------------------------------------------------------  -->

\begin{figure}
 % **Fig:multi-style-deep-concat-architectural-overview**
 \centering
 \includegraphics[width=0.8\textwidth]{source/architecture/MultiNetDeepConcatenation.pdf}
 \caption[System overview for training with low dimensional style features]{
   System overview for training a multi-style network by concatenating dimensionality reduced style features after the downsampling layers.
 }
 \label{fig:pca-system-overview}
\end{figure}

<!-- \subparagraph*{Introduction}\nop{*} -->
The idea for this method is that the transformation network is not able to copy the style image directly if it only has access to features extracted from the style image. Fortunately, we already have features that represent the style and can be used. Specifically, the same features we use for our loss function: The Gram matrix of the different outputs from our loss network. How do we provide them to our transformation network?
Every layer in a convolutional neural network has multiple feature maps and we would like all of them to have access to the style features. Therefore, we want to concatenate the style features with the feature maps. Unfortunately, the style features have a very high dimension. That is where we need dimensionality reduction.

<!-- \subparagraph*{Method}\nop{*} -->
We chose principal component analysis (PCA) [@pca1901] for performing the dimensionality reduction as it is an often used and well understood method. We wanted to apply PCA on our *Painters Style Dataset* that consists of about 80k images, which requires an enormous amount of computer memory as all of them need to be in memory for PCA. Therefore, we had to rely on incremental PCA [@IncrementalPCA2008] which is a variant of PCA with an upper bound on memory requirements. For this work we treat PCA as a black box: We give PCA all of our feature vectors $V$. PCA gives us a projection matrix $M$ and mean $\mu$ that together reduce a feature vector $v \in V$ to a version with a selected amount of entries, where the resulting entries are ordered by their variance. The more of those principal components (entries) we select, the more variance of the original data we cover. If we chose all entries, we could undo the operation without any information loss. For incremental PCA, we do not have to deliver all of our feature vectors at once, but can do so in batches.

We did the following steps to acquire our four different matrices $M_i$ and mean values $\mu_i$. One matrix per style-layer in our loss network.

1) Get a batch of $7200$ style images $s_k$ from the dataset, where $k$ is the index of the image.
2) Run every image $s_k$ through the VGG-16 loss network $\phi$. As we use four style-layers $i$ throughout this work, we get four different feature maps per image $\phi_i(x)$.
3) Apply the Gram $G$ on every feature map.
4) Reshape the resulting style loss features into vector form with reshape function $r$.
5) Apply four independent incremental PCAs, each on one of the four batches of $7200$ vectorized style loss features $F_i(x_k) = r(G(\phi_{i}(x_k))$.
6) Repeat Step 1 to 5 until all images are processed once.


We decided to use six of the principal components per style loss feature $F_i$, which results in more than 65% covered variance, see [@Tbl:covered-variance]. Consequently, the low dimensional style loss feature $P_i = ( M_{i}*(F_{i}(s)-\mu_{i}), )$ has six dimensions. We concatenate all four of them into one vector of size $24$: $P = (P_{relu1\_2}, P_{relu2\_2}, P_{relu3\_3}, P_{relu4\_3})$.

<!-- \subparagraph*{Architecture}\nop{*} -->
We create the *pca-multi-style network* $f_W$ by injecting $P$ into the single-style network after the downsampling layers and before the first residual block by repeating $P$ so it matches the size of the feature map. Dimensionality reduction and downsampling both reduce the size of the original image, so concatenation after the downsampling seems reasonable. The resulting tensor $[{P}_{\times (88 \times 88)}]$ is concatenated with all $128$ feature maps of size $88 \times 88$ including $2*7$ border pixels from the initial reflection padding. This results in the final size of $(128+24) \times 88 \times 88$. The whole system is visualized in [@Fig:pca-system-overview].

Aside from the injected reduced style features the training follows the same procedure as for our single-style baseline. For training for multi-style the procedure equals the one from the concatenation approach.



   principal components            conv1_2        conv2_2        conv3_3        conv4_3        average
-----------------------        -----------    -----------    -----------    -----------    -----------
                      6              91.7%          80.8%          58.9%          29.5%          65.2%
                     20              99.6%          93.3%          76.6%          43.4%          78.2%

 : Covered variance of PCA from the style transfer features. {#tbl:covered-variance}


 <!-- \begin{table}[]
 \centering
 \caption{My caption}
 \label{tbl:covered-variance}
 \begin{tabular}{lllll}
 \hline\hline
                         & conv1_2 & conv2_2 & conv3_3 & conv4_3 & average\\ \hline
 6 principal components  & 91.7\%   & 80.8     & 58.9     & 29.5     & 65.2\\
 20 principal components & 99.6\%   & 93.3     & 76.6     & 43.4     & 78.2\\ \hline\hline
 \end{tabular}
 \end{table} -->

<!-- the double plus symbol: +\!\!+ -->
