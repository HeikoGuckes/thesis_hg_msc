\begin{figure}
\centering
  \newcommand{\thiswidth}{2.8cm}

  \newcommand{\imgcap}[2]{%
    \begin{minipage}{\thiswidth}%
      \includegraphics[width=\thiswidth,height=\thiswidth]{#1}%
      \vspace{-0.2cm}
      \caption*{#2}%
    \end{minipage}%  
  }

  \imgcap{source/images/styles/candy.png}{
    Candy Style}
  \imgcap{source/results/naive/naive_COCO_only_b4_ls5_lc1_painters/chicagoXcandy040000.png}{
    Chicago@Candy}
  \imgcap{source/images/content/chicago.png}{
    Chicago Content}
  \imgcap{source/results/naive/naive_COCO_only_b4_ls5_lc1_painters/chicagoXfamosas-crop040000.png}{
    Chicago@Famosa}
  \imgcap{source/images/styles/famosas-crop.png}{
    Famosa Style}

  \caption[Visualization of the style blend problem on the painters dataset]{
      Results from the \dd*painters-concatenation network*. If we increase the number of styles to 80k we see the \dd*style-blend* problem. Instead of styling the \dd*Chicago* content image with the \dd*Candy* style, the network produces a blended version of both images with some color transfer.
  }
  \label{fig:concatenation-painters-shows-style-blending}
\end{figure}
