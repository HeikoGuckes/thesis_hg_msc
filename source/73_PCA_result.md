
## Evaluation of the Dimensionality Reduction Approach for Ten Styles
<!--  --------------------------------------------------------------------  -->

The idea for this method was to use a low dimensional version of the style image, created with PCA, to prevent the network from copying or blending the style image directly into the pastiche.

\subparagraph*{Visual}\nop{*} Our results show that the network produces the same pastiche for every style, so it seems to learn some kind of average style instead of multi-style. It shows the same behavior when we increase the principal components from 6 to 20.

As an additional experiment we trained the network with a single style (*Starry Night*) and the network delivered comparable results to the single-style baseline. This is not unexpected as the network can simply learn to ignore the additional information as it is constant for a single style.

\subparagraph*{Losses}\nop{*} The losses in [@fig:pca-plot-ten-styles] show that the network nearly stops learning after very few hundred parameter updates. Additionally, the style loss is extremely high compared to the content loss, indicating that the network does not optimize well for multiple styles.

\begin{figure}[h]  
  \begin{minipage}[c]{0.46\textwidth}
    \includegraphics[width=\textwidth]{source/results/pca/PCA_b4_ls5_lc1_ltv0_PCAv2GramPhiStyle_April/PCA_b4_ls5_lc1_ltv0_PCAv2GramPhiStyle_April_params__Loss_mean_on_the_validation_set_average.pdf}
  \end{minipage}
  \hfill
  \begin{minipage}[c]{0.49\textwidth}
    \captionsetup{singlelinecheck=false,format=plain,justification=raggedright,labelsep=newline}
    \caption[Loss of the dimensionality reduction approach and the supervision approach for the Ten-Styles Dataset]{
      Results from the dimensionality reduction with PCA approach and the supervision approach for the \dd*Ten-Styles Dataset*.
      We can see that the style loss is not even close to the content loss. After an extremely short initial learning phase the curve for the style loss is flatlining, so the network stops learning.
    }
    \label{fig:pca-plot-ten-styles}
  \end{minipage}
\end{figure}


\subparagraph*{Discussion}\nop{*}

The results show that the output is independent of the style input. This tells us that the network architecture is not capable to use the additional information. We see the following problems with the approach that might explain the negative results:

- There is no guarantee that the directions of maximum variance extracted from the style features by PCA contain the necessary information to discriminate between styles.

- The replicated data looks the same at every point within a feature map for a convolutional filter except for the borders. This might be the reason why the additional data has no impact at all.

- We learned the low dimensional representation directly for 80k styles images. Our success with a more limited amount of images for the other approaches indicates that this might have been too ambitious.

We conclude that this approach is not multi-style capable for ten styles and offers no advantage over the single-style network. Therefore, we exclude the results from the comparison with the state of the art single-style baseline.
