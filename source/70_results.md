\newcommand{\singlenets}{\textit{single-style networks }}
\newcommand{\singlenet}{\textit{single-style network }}
\newcommand{\penalizednet}{\textit{penalized-ten-styles-network }}
\newcommand{\controllednet}{\textit{controlled-content-ten-styles network }}
\newcommand{\combinednet}{\textit{combined-ten-styles network }}
\newcommand{\supervisednet}{\textit{supervised-ten-styles network }}
\newcommand{\naivenet}{\textit{concatenation-ten-styles network }}

\newcommand{\Ssinglenet}{\textit{single-style network }}
\newcommand{\Spenalizednet}{\textit{penalized-network }}
\newcommand{\Scontrollednet}{\textit{controlled-content network }}
\newcommand{\Scombinednet}{\textit{combined network }}
\newcommand{\Ssupervisednet}{\textit{supervised network }}
\newcommand{\Snaivenet}{\textit{concatenation network }}
\newcommand{\Siterative}{\textit{iterative method}}

\newcommand{\Isingle}{\textit{single-style}}
\newcommand{\Ipenalized}{\textit{penalized}}
\newcommand{\Icontrolled}{\textit{controlled}}
\newcommand{\Icombined}{\textit{combined}}
\newcommand{\Isupervised}{\textit{supervised}}
\newcommand{\Inaive}{\textit{concatenation}}
\newcommand{\Iiterative}{\textit{iterative}}




# Evaluation of the Novel Approaches {#sec:results}

In this section we want to evaluate our approaches. We start by outlining the methodology, then look at the results from each individual approach for ten styles to assess their viability. We then compare the viable approaches to the single-style and iterative baselines, also for ten styles. After that we extend the amount of styles to 80k.

## Methodology {#sec:methodology}

The evaluation of the novel approaches consists of two parts. The first part evaluates the individual viability. The second part compares the results of the viable approaches with the state of the art single-style and iterative approaches.

We evaluate the viability of each approach guided by three questions:

1) Are all approach components necessary?
2) Does it solve the *style copy artifacts* and *style blend* issues of the concatenation approach?
3) Is it multi-style capable for ten styles and how many parameter updates are necessary to achieve visually pleasing results?

We ask the first question to gain more insight into the internal mechanics of our approaches. As the approaches were motivated by the two issues of the concatenation approach we ask the second question to check if they work as intended. The goal for this thesis is a multi-style network. We dismiss approaches that have no benefit towards this goal with question three.


This thesis wants to achieve comparable results to multiple single-style networks using a single network. Therefore, we consider the state of the art single-style networks as the baseline and naturally have to compare all our viable approaches directly with them. The single-style networks approximate the original iterative method from @gatys. Our approaches do the same and we therefore use it as a second baseline.  

We use three different methods for evaluation:  

- Visual inspection
- Analysis of the artifacts
- Analysis of the loss function


All approaches have the goal to optimize the loss function, so using it as one method of evaluation is obvious. We already learned from the concatenation approach that a good loss function does not necessary mean a pastiche that is visually close to the single-style baseline. Therefore, we rely on visual inspection. Additionally, we found that the concatenation approach tends to produce *style copy artifacts*. They are found in every pastiche and ignore the content. To assess if an approach improves in this aspect, we measure the percentage of similar ($\pm3$) pixels in each color channel in three different content images. We can also set the non-similar colors to 0 and the similar colors to 255 to get an image representing the invariance over the three content images.


As we compare each novel approach with the concatenation approach, we use the same architecture, system setup and parameters as for training the *concatenation-multi-style network*. Differing parameters or extensions to the architecture specific to one of our novel approaches are described in [@Sec:approaches]. We conducted all experiments using the *Microsoft COCO Dataset* as content images and the *Ten-Styles Dataset* as style images.
The architecture used for the single-style networks is the one from @Johnson with three modifications that are not related to multi-style as introduced in [@Sec:single-style-architecture]. It was extended by concatenating the content and style image at the first layer to achieve multi-style as introduced in [@Sec:concatenation-approach]. The general methodology including all the parameters was outlined in [@Sec:general-methodology] and the datasets are explained in more detail in [@Sec:datasets].
















<!-- Zusammenfassung der Inhalte:

 combined:

 - penalize works for some images
 - controlled improves content visibility
 - combined they compliment each other
 - complete loss discussion
     - Loss (controlled vs penalize vs combined vs concatenation)
 - Visual
 - ++Artifacts  


supervision

 - reference only leads to blurry pastiches
 - adding style loss alon eadds Artifacts
 - adding style+contetn loss improves blurriness
 - loss discussion
 - Visual
 - ++Artifacts

PCA
 - geht nicht, siehe building
 - siehe plot


Guide:

 0) Introduction
 1) visual inspection: Are all approach components necessary?
 2) Does it solve the *style copy artifacts* and *style blend* issues of the concatenation approach using visual inspection and artifacts?
 3) Is it multi-style capable for ten styles? -->


<!--
  datensatz
  welche evaluations-methode
  was pro methode rausgekommen ist
-->


<!-- ## Angles (Draft Helper)

Baselines:

 - single-style   
 - concatenation




Angles:

 - Loss
    - Concrete values in table
    - Plots
        - show also the curves from l_ref and l_b

 - Visual
    - Perceived style quality
    - Difference to baselines
    - Sharpness?
    - Artifacts?
    - Style blend?

 - Extracted Artifacts


 - Learning speed

 -


Approaches:

 - penalized  (\penalizednet)
 - control  (\controllednet)
 - combinedPenalizeControl  (\combinednet)
 - supervise \supervisednet
 - naive \naivenet
 - single \singlenet -->

 <!-- - \Isingle
 - \Ipenalized
 - \Icontrolled
 - \Icombined
 - \Isupervised
 - \Inaive -->

<!--



 The \Ssinglenet


 \Isingle
 \Ipenalized
 \Icontrolled
 \Icombined
 \Isupervised
 \Inaive

 \Ssinglenet
 \Spenalizednet
 \Scontrollednet
 \Scombinednet
 \Ssupervisednet
 \Snaivenet -->


\newpage
