\newpage

# Theory {#sec:theory}

<!-- ##   Generative vs. discriminative models -->

This section introduces the concepts necessary to understand how a neural network is constructed and trained. For understanding most parts of this thesis they can be seen as black boxes, but having an idea of the underlying structure should make it easier.

This section starts by introducing the general neural network building blocks in [@sec:neural-network-blocks] and activation functions (@sec:activation-functions). Both are required to understand the architecture of the VGG-16 network that is used as the loss network for this thesis and to understand the neural network that generates the pastiches. After that batch normalization (@sec:Batch-Normalization) is introduced as it is an option for the single-style architecture. Data often needs specific preprocessing (@sec:preprocessing) for certain networks, like the VGG-16 network, before it can finally be used to train a neural network (@sec:Training).


## Neural Network Building Blocks {#sec:neural-network-blocks}

This section starts with a single artificial neuron (@sec:basic) and its  and then combines many of them to form a small neural network with one hidden layer (@sec:hidden-layer).

For image processing we need to have a look at convolutional layers (@sec:Convolutional-Neural-Networks). For architectures with many layers we need residual blocks ([@Sec:residual-blocks]). We look at typical layer combinations in (@sec:Combining-Different-Layers)

###	 Artificial Neuron {#sec:basic}
From a mathematical standpoint a single artificial neuron at its core is just a linear function $\sum_i (w_ix_i+b)$ where $x_i \subseteq \Re$ are called the inputs, $w_i \subseteq \Re$ the weights and $b \subseteq \Re$ the bias. This is just a sum over the weighted inputs. Before the output the sum is transformed by the so-called activation function $f$ [@rosenblatt1957perceptron]. The concept is visualized in [@Fig:BasicNeuron].
 <!-- rewrite this in vector notation to $f(Wx+b)$. -->

<!-- $$ f(\sum_i (w_ix_i+b)) $$ {#eq:single-neuron} -->

\begin{figure}[h]
  % **Fig:BasicNeuron**
  \centering
  \includegraphics[width=0.6\textwidth]{source/theory/NeuronBasic.pdf}
  \caption[Visualization of an artificial neuron]{Artificial neuron with inputs $x_i$, weights $w_i$ and output $y$}
  \label{fig:BasicNeuron}
\end{figure}

###  Neural Network With One Hidden Layer {#sec:hidden-layer}
Combining multiple artificial neurons leads to a neural network. The individual neurons usually do not share weights. If there are no loops or back connections it is called feed-forward. That type can be presented by a directed acyclic graph as in @Fig:HiddenLayer, which shows a network with two layers. The input layer is not counted as it does not have weights or an activation function. The hidden layer has no direct connection to the outside. At the output layer, the result of the networks calculations can be seen. The whole construct can be seen as a mapping where the input is transformed and mapped to the output layer. Neural networks can approximate any arbitrary function. The degree of accuracy is limited by the number of hidden layers [@Hornik1989].

\begin{figure}[h]
  % **Fig:HiddenLayer**
  \centering
  \includegraphics[width=0.4\textwidth]{source/theory/HiddenLayer.pdf}
  \caption[Example of a neural network with one hidden layer]{A neural network consisting of input, one hidden, and output layer. Multiple hidden layers are possible.}
  \label{fig:HiddenLayer}
\end{figure}

###   Convolutional Neural Networks {#sec:Convolutional-Neural-Networks}
A convolution can be seen as a filter which is moved over an image and produces a new image. The filter size is usually much smaller (e.g.\ $3 \times 3$) than the image itself. The resulting image can be seen as a feature map of the original image. For example, one could use a vertical edge detection filter and the resulting image would represent vertical edge features. Using many different filters gives multiple perspectives of the original image. This controls the number of output channels. In a second run one could apply different filters to those features to extract higher level features. This is the essential idea of a convolutional neural network (CNN). CNNs are often used for image classification [@imagenet2012].

The weights of a CNN are the values of the convolution filter, which is applied to the whole image. This can be seen as the same filter applied to the whole image or multiple filters using the same weight. In this sense weights are shared. We can view the result of the filter as a feature map and CNNs usually contain many feature maps per layer.

Other parameters are the size of the filter (kernel size) and if the filter is applied at each pixel (stride = 1) or every other pixel (stride = 2).

### Residual Blocks {#sec:residual-blocks}
Given two layers that can be seen as a function $\mathcal{F}(x)$, the original proposed residual building block from @residual is simply $\mathcal{F}(x) + x$. The addition of the input can be seen as a shortcut for the data flowing into the block. The main goal of this technique is to allow deeper (more layers) networks. The intuition is that in case of the input to the block being already optimal, the network can just learn to set all weights underlying $F$ to zero and do nothing. This should be easier than learning the identity function to achieve the same.

###   Combining Different Layers {#sec:Combining-Different-Layers}
In practice different types of layers are combined to form a more powerful network. Usually one finds (e.g.\ [@vgg2014]):

- Convolutional layers (@Sec:Convolutional-Neural-Networks): usually one or more successively to produce feature maps.

- Pooling layers or subsampling layers: this type of layer often uses non linear filters compared to convolutional filters. They are applied to every part of the image. For example, the frequently found max-pooling layer selects a maximum to discard irrelevant information from a feature map or to simply reduce the size. A typical filter size is $2 \times 2$ or $3 \times 3$.

- Fully connected layers (@Sec:hidden-layer): After multiple occurrences of the former mentioned layers there are usually multiple fully connected layers. They can use the resulting feature maps of a convolutional layer to classify objects. The amount of neurons in the last fully connected layer often corresponds to the amount of classes you want to learn. If you have a class for fire trucks, the corresponding neuron should only be active when it detects a fire truck. Ideally all others should be inactive in this case.
<!-- and prevent overfitting -->



##   Activation Functions {#sec:activation-functions}

This part presents a minimalistic overview over the most common activation functions. Activation functions can introduce nonlinearity to a neural network. Without them, or by choosing the identity function, the resulting network would just be a linear combination of weights and inputs. To produce a non-linear decision boundary when used for classification, a neural network requires non-linear combinations of the weighted inputs.


\begin{figure}
  % **Fig:ActivationFunctions**
  \centering
  \includegraphics[width=0.5\textwidth]{source/theory/ActivationFunctions.pdf}
  \caption[Common activation functions]{Visual comparison of common activation functions: hyperbolic tangent, sigmoid, rectified linear (ReLU) and its leaky variant}
  \label{fig:ActivationFunctions}
\end{figure}


\subparagraph*{Sigmoid Functions} The most common sigmoid functions are the standard logistic function (@Eq:sigmoid) and the hyperbolic tangent (@Eq:tanh). Both are symmetric about the origin. The standard logistic function squashes the output into range (0, 1). The hyperbolic tangens squashes the output into the range (-1, 1). In the context of training neural networks it often converges faster due to having stronger gradients [@LeCun1998]. Both limit the output range, which is a useful property if you want to store the output as an image, which is usually limited to pixel values in the range of [0,255]. Additionally they are differentiable at every point.

$$ f(x) = \frac{1}{1+e^{-x}} $$ {#eq:sigmoid}

$$ tanh(x) = \frac{sinh(x)}{cosh(x))} = \frac{e^{x}-e^{-x}}{e^{x}+e^{-x}} = \frac{e^{2x}-1}{e^{2x}+1} $$ {#eq:tanh}



\subparagraph*{ReLU} The rectified linear unit's function (ReLU) (@Eq:relu) increases the non-linearity of the network. It is not linear and not differentiable, which clearly sets it apart from the sigmoid. Producing true zeros allows the creation of sparse representations. This property seems useful as most data contains a lot of information that needs to be ignored. The training performance is often equal or better than the above mentioned sigmoid functions [@GlorotReLU].

$$ f(x) = max(0,x) $$ {#eq:relu}


\subparagraph*{Leaky ReLU} This variant of the ReLU (@Eq:leakyrelu) sacrifices the true zero sparsity. It has the advantage that a once learned zero is not stuck in the network forever since the gradient is not zero. Therefore, it can be more robust during optimization  [@Maas2013RectifierNI].

$$ f(x) = max(0.01x,x) $$ {#eq:leakyrelu}

@Fig:ActivationFunctions gives graphical representation for comparing all the presented activation functions.









##   VGG-16 {#sec:vgg-16}

The Oxford Visual Geometry Group (VGG) proposed the VGG network for object recognition in images [@vgg2014]. It is used extensively in this thesis to evaluate the quality of a pastiche and is a great example for layer combinations. The VGG-16 variant consists of five parts with either two or three convolutional layers with ReLU nonlinearities followed by a pooling layer. The pooling layers halve the size but double the amount of the feature maps. After the five convolutional parts there is a part consisting of three fully connected layers without convolutions. We can see the first five parts as feature extractors and the fully connected part is using those to discriminate between objects. [@Fig:vgg-16-only] illustrates the 16-layer variant. Only the first four parts are used as the loss network for this thesis.

\begin{figure}
  % **Fig:VGGNet**
  \centering
  \includegraphics[width=0.8\textwidth]{source/architecture/VGGNet.pdf}
  \caption[Overview of the VGG-16 network]{Overview of the VGG-16 network. The first 4 parts are used as the loss network for this thesis.}
  \label{fig:vgg-16-only}
\end{figure}

##   Preprocessing {#sec:preprocessing}

In machine learning data is often preprocessed using methods like whitening or principal component analysis (PCA) [@pca1901]. In image processing some of these methods are not so useful, since images do not have separate features with different units. Color images have 3 channels of equal size and all values are between and including 0 and 255. In some cases there is a so-called mean image which has to be subtracted from every image to center the data around 0. In other cases (e.g.\ VGG-16) there is a per-channel mean where a value has to be subtracted from every channel of an image [@vgg2014]. Centering the data like this has the advantage that the gradients of a sigmoid function in the network have their biggest value and the network might converge faster. Being at the edges of the sigmoid will lead to very small gradients and learning will potentially be slow.


##   Batch Normalization Layer {#sec:Batch-Normalization}
<!-- https://www.youtube.com/watch?v=gYpoJMlgyXA&feature=youtu.be&list=PLkt2uSq6rBVctENoVBg1TpCC7OQi31AlC&t=3078 -->
Instead of processing data one at a time, it is often bundled into small batches, e.g.\ four images together. Averaging over such a batch reduces the impact of extreme outliers in the data. Additionally, data is often preprocessed in a way that it has zero-mean and unit variance before it is fed into a machine learning algorithm. This allows it to relate values or units of different scale. Due to all the scaling in a neural network this property might get lost. This is called the internal covariate shift. To avoid this, a batch normalization layer [@batchnorm2015] can be used. It normalizes the data over each batch, so the next layer gets input with zero mean and unit variance.

Given a batch with activations $x^{(k)}$ the normalized value is calculated with @Eq:batch-norm, where $E$ is the mean and $Var$ is the variance of the batch:

$$ \hat x^{(k)} = \frac{  x^{k} - E[x^{k}]  }{  \sqrt( Var[x^{k}])  }
$$ {#eq:batch-norm}

Using this normalized value the network might be limited in what it can represent, so the authors introduce two learnable parameters for the layer that allows to scale $\gamma$ and shift $\beta$ the normalized value $\hat x^{(k)}$:

$$ y^{(k)}=\gamma^{(k)}\hat{x}^{(k)}+\beta^{(k)}.
$$ {#eq:batch-norm-scale-shift}

These parameters allow the network to learn to undo the normalization completely when this is optimal.

Due to the normalization over a whole batch, outliers do not have a high impact, so higher learning rates may be possible and the type of initialization is less important [@batchnorm2015].

##   Training {#sec:Training}

Training a neural network requires several components. First, the weights need to be initialized somehow. Second, a batch of training data can be fed into the network and the error of the result needs to be evaluated by using a loss function. Third, the final error is used to determine the contribution of every weight by using backpropagation. Finally, the optimizer can adjust the weights of the network to reduce the error. These four steps update the parameters one time and are usually repeated thousands of times until the result is satisfying.


###      Initialization

Initialization of the weights of a neural network is not as simple as one might think. If all weights are initialized with zero then all intermediate errors would be zero too as they are also weighted by the weights. Thus, the network could not learn. Initializing all of them with the same non zero value would lead to all of the intermediate results to be equal. All errors would be equal too, so the symmetry has to be broken.

Sampling the initial weights from the standard normal distribution is an option, but the parameters need to be selected carefully. With too low variance the gradients will die and with too high variance the neurons might oversaturate and all of the gradients would be near 1 and learning again would be very slow. A typical initialization scheme uses a standard deviation of 0.01 with zero mean. We want to at least mention the well known Xavier initialization for completeness [@Glorot2010], but it is not required for this thesis due to the effective normalization strategies.


### Per-Pixel Loss Function {#sec:loss-function}

\newcommand{\el}{\mathcal{L}}

A loss function $\el$ quantifies how unsatisfying the output is that the network generated from the data. It is often called cost or objective function in the literature.

If the desired result is already known, one could use it as a reference $y$ for the output image $\hat y$ which both have the same number of pixels. Punishing deviation on a pixel basis is called per-pixel loss:

$$
\el_{pixel}(\hat y, y) = \frac{1}{N}   \Vert  \hat y - y  \Vert_{2}^{2}
$$ {#eq:per-pixel-loss}

where $N$ is just a normalizing factor representing the number of pixels in $y$.



###      Backpropagation {#sec:backpropagation}

Backpropagation is an algorithm for training neural networks that requires only one forward and one backward pass to calculate the error for all weights [@Rumelhart1986]. We will have a look at its error propagation.
The loss function calculates the error $L$ at the final layer. We are looking at weight $w^l_{jk}$ for the $k^{th}$ neuron in the $(l-1)^{th}$ layer to the $j^{th}$ neuron in the $l^{th}$ layer. We want to know how much this weight influences the loss function $L$. We can do this by applying chain rule (eq:chain-rule) if we know the activation $a_j$ of the *next* neuron. Fortunately, we know the final loss (final activation), so we propagate it backwards by repeatedly using chain rule to calculate the error for every weight.

$$ \delta^l_j
= \frac{\delta L}{\delta w^l_{jk}}
= \frac{\delta L}{\delta a_j}   \frac{\delta a_j}{\delta w^l_{jk}} $$ {#eq:chain-rule}


###      Adam Optimizer {#sec:adam-optimizer}

<!-- http://sebastianruder.com/optimizing-gradient-descent/index.html#fn:3 -->
<!-- https://moodle2.cs.huji.ac.il/nu15/pluginfile.php/316969/mod_resource/content/1/adam_pres.pdf -->
<!-- https://www.quora.com/Can-you-explain-basic-intuition-behind-ADAM-a-method-for-stochastic-optimization -->
<!-- http://www.cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf -->


Now that the error is known for all the weights, they can be written in a vector $\bm w$ and an optimizer can update their value. A very common way is to use stochastic gradient descent (SGD) where the (scaled with $v$) error $\Delta$ from a single training sample $L_i$ is simply subtracted from the weights:
<!-- (@Eq:sgd): -->

$$ \bm w^{t+1} = \bm w^t - \nu \Delta_w L_i(\bm w)$$ {#eq:sgd}

Many enhancements to this method have been proposed. One of the most common is the so-called momentum [@momentum2013]. The basic idea is to incorporate a part of the last update changes into the current update. This smoothes and speeds up the updates and can prevent oscillating behavior and convergence to local minima. To achieve this the last weight update $\nabla \bm w$ is multiplied by the momentum $\mu$ to @Eq:sgd :

$$ \bm w^{t+1} = \bm w^t - \nu \Delta_w L_i(\bm w) + \mu \nabla \bm w^t $$ {#eq:momentum}

To tackle different problems further methods have been proposed:

- Nesterovs' Accelerated Gradient (NAG), which may improve stability and speed in ill-conditioned problems [@momentum2013].
- Adaptive Gradient (AdaGrad), which increases the learning rate for rarely varying parameter and suppresses highly varying parameters. It weights older and newer gradients the same, which is a problem when the setting changes. [@adagrad2011]
- Root Mean Square Propagation (RMSProp), which is similar to AdaGrad, but uses an exponential moving average controlled by a parameter that can emphasize more recent gradients. It works better than AdaGrad when the setting is changing.

Adaptive Moment Estimation (Adam) tries to combine the advantages of AdaGrad and RMSProp, which store an exponentially decaying average of past squared gradients (second moment). Adam additionally keeps track of an exponentially decaying average of past first moment. The method works very well in practice compared to other adaptive learning algorithms [@adam]. Therefore, it is used for training our networks.


<!-- are estimates of the first moment (the mean) and the second moment (the uncentered variance) -->
