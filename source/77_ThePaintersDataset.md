\newpage

## Evaluation of the Generalizablity with More than Ten Styles {#sec:painters-results}

In this section we evaluate the viability of our approaches for more than ten styles. Additionally, we want to assess the ability of the learned networks to generalize to unlearned styles. While the supervision approach produced viable results close to the single-style networks it requires those networks for training. Due to limited computational resources we had to exclude the supervision approach for more than 10 styles. Therefore, we conducted these experiments using exclusively the combined approach. The only variable is the training dataset.

To determine the amount of styles a network can learn before the quality starts to decline, we sample four smaller dataset from the *Painters Dataset* with 500, 200, 100 and 31 style images. All bigger datasets contain all images that the smaller datasets contain. We add the *Starry Night* style image to every dataset to have a quality reference to our *Ten-Styles Dataset* that we use as the baseline for this experiment. The full datasets are explained in more detail in [@Sec:datasets].

\subparagraph*{Visual}\nop{*} In [@fig:visual-combined-generalizability] we can see that for the ten and 32 styles datasets the pastiches from the \dd*Starry Night* style image have equal quality. The quality declined using 101 style images and regressed to color transfer and \dd*style blending* for 201 and more images.   
The networks that were learned with more than ten styles did not train on the \dd*Famosa* style, but they are still able to generate pastiches close to the results from our \dd*combined-ten-styles network*. All other input styles produced results that we do not consider close, but we see more than simple *style blending*.
Comparing the results from the full 80k *Painters Dataset* in [@fig:visual-combined-approach-painters] with the *concatenation approach* we can see that the *style blend* problem is not alleviated. The content image is more prominent for the combined approach. Colors are transferred quite well, since the *Famosa* Style image produces a black and white result. Using the mean image over three content images shows the closeness of the pastiches to the style image. The main difference to the single-style or ten-styles networks is that the content is not transformed or distorted by the style. In other words, the content does not get styled. This problem is also not alleviated by increasing the $\lb$ from the combined approach, but additionally leads to wrong colors being transferred.

\subparagraph*{Artifacts}\nop{*}
All results contain less than 2.5% similar pixels. This underpins that the network is not simply outputting the style image, but blends both images together.

\subparagraph*{Losses}\nop{*}
Looking at the losses in [@Fig:loss-combined-painters-samples] we can see that the dataset with 201 images results in a better style loss than the one with 101 images. However, this results in a worse absolute blend loss. The decreasing blend loss with more parameter updates indicates that the datasets with more than 101 images learn to blend the style image. We do not show the content loss as it is unremarkable. It gets worse the bigger the datasets gets and its absolute value is quite low, so the total loss is dominated by the style loss. This is consistent with the observation that the content is less intensive in pastiches from networks learned with more images. Interestingly, increasing the blend loss by increasing $\lb$ does not improve the results (not shown).

\subparagraph*{Discussion}\nop{*}
It seems that, despite explicitly penalizing the closeness to the style image, the *style blend* strategy seems to be the best when the amount of images is increased. There are styles where the single-style approach produces pastiches that only vary slightly in color (weak styling). The *Ten-Styles Dataset* contains styles that were selected for comparison with previous work, which indicates that those styles produced interesting results in the first place and are well suited for the *perceptual loss function*. The *Painters Dataset* on the other hand does not contain "well suited" styles but a broad collection. It might be that styles with weak styling reduce the overall result. Another explanation is that certain styles cancel each other out. One style might encourage the network to learn warped edges, where another encourages the network to learn the original edge.

\begin{figure}
  \centering
  \includegraphics[height=0.24\textheight]{source/results/many/plot_Style_loss_mean_on_the_validation_set_starry.pdf}
  \hfill
  \includegraphics[height=0.24\textheight]{source/results/many/plot_Blend_loss_mean_on_the_validation_set_starry.pdf}
  \caption[Losses for the combined approach on the Painters Dataset]{
     Networks learned with \dd*combined approach* on different datasets sampled from the \dd*Painters Dataset* plus the \dd*Starry Night* style. The dataset with 201 images results in a lower style loss than the one with 101 images (left plot). However, this is results in a worse absolute blend loss (right plot), so the datasets with more than 101 images actually learn to blend the style image as it gets worse with more updates.
  }  
  \label{fig:loss-combined-painters-samples}
\end{figure}
